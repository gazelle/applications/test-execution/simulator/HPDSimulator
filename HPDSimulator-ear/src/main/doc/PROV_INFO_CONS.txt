@startuml
hide footbox
title ITI-58: Provider Information Query
participant CONS as "PROV_INFO_CONS  (HPD Simulator)" #99FF99
participant DIR as "PROV_INFO_DIR  (SUT)"
CONS -> DIR: Provider Information Query Request
activate DIR
DIR --> CONS: Provider Information Query Response
deactivate DIR 
@enduml