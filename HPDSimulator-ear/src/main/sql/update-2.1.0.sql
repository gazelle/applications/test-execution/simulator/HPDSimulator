delete from app_configuration WHERE variable = 'cas_url';
INSERT INTO app_configuration(id, variable, value) VALUES (nextval('app_configuration_id_seq'), 'restrict_access_to_messages', 'false');
UPDATE "hpd-simulator".public.cmn_transaction_instance SET company_keyword = username;
ALTER TABLE "hpd-simulator".public.cmn_transaction_instance DROP COLUMN username;

ALTER TABLE cmn_transaction_instance ADD COLUMN new_standard VARCHAR(64);
UPDATE cmn_transaction_instance set new_standard = 'HL7V2' where standard = 0;
UPDATE cmn_transaction_instance set new_standard = 'HL7V3' where standard = 1;
UPDATE cmn_transaction_instance set new_standard = 'XDS' where standard = 2;
UPDATE cmn_transaction_instance set new_standard = 'FHIR_XML' where standard = 3;
UPDATE cmn_transaction_instance set new_standard = 'FHIR_JSON' where standard = 4;
UPDATE cmn_transaction_instance set new_standard = 'SVS' where standard = 5;
UPDATE cmn_transaction_instance set new_standard = 'HPD' where standard = 6;
UPDATE cmn_transaction_instance set new_standard = 'OTHER' where standard = 7;
ALTER TABLE cmn_transaction_instance DROP COLUMN standard;
ALTER TABLE cmn_transaction_instance RENAME new_standard TO standard;