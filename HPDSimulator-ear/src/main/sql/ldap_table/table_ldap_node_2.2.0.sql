--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: ldap_node; Type: TABLE; Schema: public; Owner: gazelle; Tablespace: 
--

CREATE TABLE ldap_node (
    id integer NOT NULL,
    description text,
    name character varying(255) NOT NULL
);


ALTER TABLE ldap_node OWNER TO gazelle;

--
-- Data for Name: ldap_node; Type: TABLE DATA; Schema: public; Owner: gazelle
--

COPY ldap_node (id, description, name) FROM stdin;
1	Healthcare Professional	HCProfessional
2	Healthcare organizations	HCRegulatedOrganization
3	Relationships between healthcare professionals and organizations	Relationship
4	credentials	HPDProviderCredential
5	Membership	HPDProviderMembership
6	Electronic services	HPDElectronicService
\.


--
-- Name: ldap_node_name_key; Type: CONSTRAINT; Schema: public; Owner: gazelle; Tablespace: 
--

ALTER TABLE ONLY ldap_node
    ADD CONSTRAINT ldap_node_name_key UNIQUE (name);


--
-- Name: ldap_node_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle; Tablespace: 
--

ALTER TABLE ONLY ldap_node
    ADD CONSTRAINT ldap_node_pkey PRIMARY KEY (id);


--
-- PostgreSQL database dump complete
--

