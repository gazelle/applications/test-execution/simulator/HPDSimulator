--
-- PostgreSQL database dump
--

-- Dumped from database version 9.6.7
-- Dumped by pg_dump version 9.6.7

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: hpd_ldap_attribute_types; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE hpd_ldap_attribute_types (
    id integer NOT NULL,
    description character varying(255),
    name character varying(255),
    oid character varying(255),
    equality integer,
    length integer,
    ordering integer,
    singlevalue boolean,
    substr integer,
    syntax character varying(255)
);


ALTER TABLE hpd_ldap_attribute_types OWNER TO gazelle;

--
-- Data for Name: hpd_ldap_attribute_types; Type: TABLE DATA; Schema: public; Owner: gazelle
--

COPY hpd_ldap_attribute_types (id, description, name, oid, equality, length, ordering, singlevalue, substr, syntax) FROM stdin;
1	PKCS #7 ContentInfo PDU	pKCS7PDU	1.2.840.113549.1.9.25.5	\N	0	\N	f	\N	1.3.6.1.4.1.1466.115.121.1.5
2	pseudonym	pseudonym	2.5.4.65	\N	0	\N	f	\N	1.3.6.1.4.1.1466.115.121.1.15
3	PKCS #7 signing time attribute	signingTime	1.2.840.113549.1.9.5	\N	0	\N	t	\N	1.2.840.113549.1.9.26.2
4	Gender	gender	1.3.6.1.5.5.7.9.3	0	0	\N	t	\N	1.3.6.1.4.1.1466.115.121.1.44
6	PKCS #8 encrypted private key info	encryptedPrivateKeyInfo	1.2.840.113549.1.9.25.2	\N	0	\N	f	\N	1.3.6.1.4.1.1466.115.121.1.5
7	PKCS #15 token PDU	pKCS15Token	1.2.840.113549.1.9.25.1	\N	0	\N	f	\N	1.3.6.1.4.1.1466.115.121.1.5
8	PKCS #9 unstructured name	unstructuredName	1.2.840.113549.1.9.2	\N	0	\N	f	\N	1.2.840.113549.1.9.26.1
9	PKCS #9 unstructured address	unstructuredAddress	1.2.840.113549.1.9.8	0	0	\N	f	\N	1.3.6.1.4.1.1466.115.121.1.15
10	PKCS #7 content type attribute	contentType	1.2.840.113549.1.9.3	5	0	\N	t	\N	1.3.6.1.4.1.1466.115.121.1.38
11	Challenge password for certificate revocations	challengePassword	1.2.840.113549.1.9.7	\N	0	\N	t	\N	1.3.6.1.4.1.1466.115.121.1.15
12	PKCS #7 message digest attribute	messageDigest	1.2.840.113549.1.9.4	3	0	\N	t	\N	1.3.6.1.4.1.1466.115.121.1.5
13	Country of citizenship	countryOfCitizenship	1.3.6.1.5.5.7.9.4	0	0	\N	f	\N	1.3.6.1.4.1.1466.115.121.1.44
14	Place of birth	placeOfBirth	1.3.6.1.5.5.7.9.2	\N	0	\N	t	\N	1.3.6.1.4.1.1466.115.121.1.15
15	PKCS #7 counter signature	counterSignature	1.2.840.113549.1.9.6	\N	0	\N	f	\N	1.3.6.1.4.1.1466.115.121.1.5
16	Date of birth	dateOfBirth	1.3.6.1.5.5.7.9.1	2	0	\N	t	\N	1.3.6.1.4.1.1466.115.121.1.24
17	Country of residence	countryOfResidence	1.3.6.1.5.5.7.9.5	0	0	\N	f	\N	1.3.6.1.4.1.1466.115.121.1.44
18		cNAMERecord	0.9.2342.19200300.100.1.31	\N	\N	\N	f	\N	1.3.6.1.4.1.1466.115.121.1.26
19	RFC1274: Single Level Quality	singleLevelQuality	0.9.2342.19200300.100.1.50	\N	\N	\N	t	\N	1.3.6.1.4.1.1466.115.121.1.13
20	RFC1274: mail preference option	mailPreferenceOption	0.9.2342.19200300.100.1.47	\N	\N	\N	f	\N	1.3.6.1.4.1.1466.115.121.1.27
21	RFC1274: pager telephone number	pager	0.9.2342.19200300.100.1.42	4	\N	\N	f	2	1.3.6.1.4.1.1466.115.121.1.50
22	RFC1274: pager telephone number	pagerTelephoneNumber	0.9.2342.19200300.100.1.42	4	\N	\N	f	2	1.3.6.1.4.1.1466.115.121.1.50
23		nSRecord	0.9.2342.19200300.100.1.29	\N	\N	\N	f	\N	1.3.6.1.4.1.1466.115.121.1.26
24	RFC1274: DN of manager	manager	0.9.2342.19200300.100.1.10	1	\N	\N	f	\N	1.3.6.1.4.1.1466.115.121.1.12
25	RFC1274: unique identifier of document	documentIdentifier	0.9.2342.19200300.100.1.11	0	\N	\N	f	0	1.3.6.1.4.1.1466.115.121.1.15
26	RFC1274: home telephone number	homePhone	0.9.2342.19200300.100.1.20	4	\N	\N	f	2	1.3.6.1.4.1.1466.115.121.1.50
27	RFC1274: home telephone number	homeTelephoneNumber	0.9.2342.19200300.100.1.20	4	\N	\N	f	2	1.3.6.1.4.1.1466.115.121.1.50
28	RFC1274: friendly country name	co	0.9.2342.19200300.100.1.43	0	\N	\N	f	0	1.3.6.1.4.1.1466.115.121.1.15
29	RFC1274: friendly country name	friendlyCountryName	0.9.2342.19200300.100.1.43	0	\N	\N	f	0	1.3.6.1.4.1.1466.115.121.1.15
30	RFC1274: DIT Redirect	dITRedirect	0.9.2342.19200300.100.1.54	1	\N	\N	f	\N	1.3.6.1.4.1.1466.115.121.1.12
31	RFC1274: unique identifer	uniqueIdentifier	0.9.2342.19200300.100.1.44	0	\N	\N	f	\N	1.3.6.1.4.1.1466.115.121.1.15
32	RFC1274: room number	roomNumber	0.9.2342.19200300.100.1.6	0	\N	\N	f	0	1.3.6.1.4.1.1466.115.121.1.15
33	RFC1274: photo (G3 fax)	photo	0.9.2342.19200300.100.1.7	\N	\N	\N	f	\N	1.3.6.1.4.1.1466.115.121.1.23
34	RFC1274: title of document	documentTitle	0.9.2342.19200300.100.1.12	0	\N	\N	f	0	1.3.6.1.4.1.1466.115.121.1.15
35		otherMailbox	0.9.2342.19200300.100.1.22	\N	\N	\N	f	\N	1.3.6.1.4.1.1466.115.121.1.39
36	RFC1274: Subtree Maximun Quality	subtreeMaximumQuality	0.9.2342.19200300.100.1.52	\N	\N	\N	t	\N	1.3.6.1.4.1.1466.115.121.1.13
37	RFC1274: name of building	buildingName	0.9.2342.19200300.100.1.48	0	\N	\N	f	0	1.3.6.1.4.1.1466.115.121.1.15
112	RFC2079: Uniform Resource Identifier with optional label	labeledURI	1.3.6.1.4.1.250.1.57	\N	\N	\N	f	\N	1.3.6.1.4.1.1466.115.121.1.15
113		nameForms	2.5.21.7	\N	\N	\N	f	\N	1.3.6.1.4.1.1466.115.121.1.35
114	RFC2252: DIT content rules	dITContentRules	2.5.21.2	\N	\N	\N	f	\N	1.3.6.1.4.1.1466.115.121.1.16
115	RFC3045: version of implementation	vendorVersion	1.3.6.1.1.5	\N	\N	\N	t	\N	1.3.6.1.4.1.1466.115.121.1.15
116	RFC2252: matching rule uses	matchingRuleUse	2.5.21.8	\N	\N	\N	f	\N	1.3.6.1.4.1.1466.115.121.1.31
117		subtreeSpecification	2.5.18.6	\N	\N	\N	t	\N	1.3.6.1.4.1.1466.115.121.1.45
118	RFC2252: name of controlling subschema entry	subschemaSubentry	2.5.18.10	1	\N	\N	t	\N	1.3.6.1.4.1.1466.115.121.1.12
119		supportedFeatures	1.3.6.1.4.1.4203.1.3.5	5	\N	\N	f	\N	1.3.6.1.4.1.1466.115.121.1.38
120	RFC2589: entry time-to-live	entryTtl	1.3.6.1.4.1.1466.101.119.3	\N	\N	\N	t	\N	1.3.6.1.4.1.1466.115.121.1.27
121	X.501: entry has children	hasSubordinates	2.5.18.9	\N	\N	\N	t	\N	1.3.6.1.4.1.1466.115.121.1.7
122	RFC2252: name of last modifier	modifiersName	2.5.18.4	1	\N	\N	t	\N	1.3.6.1.4.1.1466.115.121.1.12
123	RFC2252: alternative servers	altServer	1.3.6.1.4.1.1466.101.120.6	\N	\N	\N	f	\N	1.3.6.1.4.1.1466.115.121.1.26
124	RFC2256: common supertype of name attributes	name	2.5.4.41	0	\N	\N	f	0	1.3.6.1.4.1.1466.115.121.1.15
125	RFC2252: DIT structure rules	dITStructureRules	2.5.21.1	\N	\N	\N	f	\N	1.3.6.1.4.1.1466.115.121.1.17
126	X.500(93): structural object class of entry	structuralObjectClass	2.5.21.9	5	\N	\N	t	\N	1.3.6.1.4.1.1466.115.121.1.38
127	RFC2252: object classes	objectClasses	2.5.21.6	\N	\N	\N	f	\N	1.3.6.1.4.1.1466.115.121.1.37
128	RFC2256: common supertype of DN attributes	distinguishedName	2.5.4.49	1	\N	\N	f	\N	1.3.6.1.4.1.1466.115.121.1.12
129	namedref: subordinate referral URL	ref	2.16.840.1.113730.3.1.34	\N	\N	\N	f	\N	1.3.6.1.4.1.1466.115.121.1.15
130	RFC2252: naming contexts	namingContexts	1.3.6.1.4.1.1466.101.120.5	\N	\N	\N	f	\N	1.3.6.1.4.1.1466.115.121.1.12
131	RFC2252: matching rules	matchingRules	2.5.21.4	\N	\N	\N	f	\N	1.3.6.1.4.1.1466.115.121.1.30
193	Values are defined in table 3.58.4.1.2.3-1	credentialStatus	1.3.6.1.4.1.19376.1.2.4.2.7	0	0	\N	t	\N	1.3.6.1.4.1.1466.115.121.1.15
194		hpdProviderPracticeAddress	1.3.6.1.4.1.19376.1.2.4.1.4	0	0	\N	f	0	1.3.6.1.4.1.1466.115.121.1.41
195	Reference for Organizational Provider (R)	hpdHasAnOrg	1.3.6.1.4.1.19376.1.2.4.1.22	1	0	\N	t	\N	1.3.6.1.4.1.1466.115.121.1.12
196	Name of Credential, degree, or certification that belongs to providerFollows\n            the ISO21091 naming format as that of the\n            HCStandardRole:credentialName@organization_domain_namewhere credentialName is the\n            standard name of 	credentialName	1.3.6.1.4.1.19376.1.2.4.2.2	0	0	\N	t	\N	1.3.6.1.4.1.1466.115.121.1.15
197		hpdProviderBillingAddress	1.3.6.1.4.1.19376.1.2.4.1.3	0	0	\N	f	0	1.3.6.1.4.1.1466.115.121.1.41
198		hpdProviderMailingAddress	1.3.6.1.4.1.19376.1.2.4.1.7	0	0	\N	f	0	1.3.6.1.4.1.1466.115.121.1.41
199	Detailed Health related credentials earned by provider. DN to one or more\n            credential entries in the HPDProviderCredential class	hpdCredential	1.3.6.1.4.1.19376.1.2.4.1.8	1	0	\N	f	\N	1.3.6.1.4.1.1466.115.121.1.12
200	Date when credential is due renewal	credentialRenewalDate	1.3.6.1.4.1.19376.1.2.4.2.6	2	0	0	t	\N	1.3.6.1.4.1.1466.115.121.1.24
201	Group to which provider is a member of. A provider can be a member of zero, one\n            or many groups.	memberOf	1.3.6.1.4.1.19376.1.2.4.1.6	1	0	\N	f	\N	1.3.6.1.4.1.1466.115.121.1.12
202	A coded value set which describes the Security Profile of a s ervice. Stored as\n            a delimited string, like and address: SecurityProfile%S\n            ecurityProfileVersion$SecurityProfileConstraints. Values are defined thro ugh local\n          	hpdSecurityProfile	1.3.6.1.4.1.19376.1.2.4.1.31	0	0	\N	f	\N	1.3.6.1.4.1.1466.115.121.1.15
203	Reference for Individual Provider (R)	hpdHasAProvider	1.3.6.1.4.1.19376.1.2.4.1.21	1	0	\N	t	\N	1.3.6.1.4.1.1466.115.121.1.12
204	A coded value set whice describes the integration profile. Store as a delimited\n            string, like and address:\n            IntegrationProfile%IntegrationProfileVersion$IntegrationProfileOptions. Values are\n            defined through local configu	hpdIntegrationProfile	1.3.6.1.4.1.19376.1.2.4.1.26	0	0	\N	f	\N	1.3.6.1.4.1.1466.115.121.1.15
205	date from which new mail will be rejected	hpdRejectMailFrom	1.3.6.1.4.1.1466.115.121.1.1.6	2	0	0	t	\N	1.3.6.1.4.1.1466.115.121.1.24
206		hpdProviderLegalAddress	1.3.6.1.4.1.19376.1.2.4.1.29	0	0	\N	f	0	1.3.6.1.4.1.1466.115.121.1.41
207	Electronic mailing address of provider where medical records can be\n            sent	hpdMedicalRecordsDeliveryEmailAddress	1.3.6.1.4.1.19376.1.2.4.1.5	0	0	\N	t	\N	1.3.6.1.4.1.1466.115.121.1.15
208	Type of Credential<degree, certificate, credential>	credentialType	1.3.6.1.4.1.19376.1.2.4.2.1	0	0	\N	t	\N	1.3.6.1.4.1.1466.115.121.1.15
209	Public Digital Certificate for this service	hpdCertificate	1.3.6.1.4.1.19376.1.2.4.1.28	3	0	\N	f	\N	1.3.6.1.4.1.1466.115.121.1.40
210	Individual-Organization Electroinc Service Information	hpdHasAService	1.3.6.1.4.1.19376.1.2.4.1.23	1	0	\N	f	\N	1.3.6.1.4.1.1466.115.121.1.12
211	The electronic service address possibly in URI or email address\n            form	hpdServiceAddress	1.3.6.1.4.1.19376.1.2.4.1.25	0	0	\N	t	\N	1.3.6.1.4.1.1466.115.121.1.15
212	Date when credential was issued to the provider	credentialIssueDate	1.3.6.1.4.1.19376.1.2.4.2.5	2	0	0	t	\N	1.3.6.1.4.1.1466.115.121.1.24
213	An identifier assigned by the provider directory whose purpose is to uniquely\n            identify a unique Credential Object	hpdCredentialId	1.3.6.1.4.1.19376.1.2.4.1.30	0	0	\N	t	\N	1.3.6.1.4.1.1466.115.121.1.15
214	Additional information on the credential	credentialDescription	1.3.6.1.4.1.19376.1.2.4.2.4	0	0	\N	t	\N	1.3.6.1.4.1.1466.115.121.1.15
215	Credential Identifier Follows the ISO 21091 UID format: (Issuing Authority OID:\n            ID) The issuing authority OID could be used to identify the issuing agency, state and\n            country.	credentialNumber	1.3.6.1.4.1.19376.1.2.4.2.3	0	0	\N	t	\N	1.3.6.1.4.1.1466.115.121.1.15
216	Languages that the provider supports. Recommended best practice is to use RFC\n            3066 [RFC 3066] which, in conjunction with ISO 639 [ISO639], defines two- and\n            three-letter primary language tags with optional subtags. Examples include	hpdProviderLanguageSupported	1.3.6.1.4.1.19376.1.2.4.1.2	0	0	\N	f	0	1.3.6.1.4.1.1466.115.121.1.15
217	Maintain status of provider in directory. Values are defined in table\n            3.58.4.1.2.3-1	hpdProviderStatus	1.3.6.1.4.1.19376.1.2.4.1.1	0	0	\N	t	\N	1.3.6.1.4.1.1466.115.121.1.15
218	An identifier assigned by the provider directory whose purpose is to uniquely\n            identify a unique Electronic Service Object	hpdServiceId	1.3.6.1.4.1.19376.1.2.4.1.24	0	0	\N	t	\N	1.3.6.1.4.1.1466.115.121.1.15
219	Unique Identifier for Membership	hpdMemberId	1.3.6.1.4.1.19376.1.2.4.1.20	0	0	\N	t	\N	1.3.6.1.4.1.1466.115.121.1.15
220	A coded value set whice describes the content profile. Store as a delimited\n            string, like and address:\n            ContentProfile%ContentProfileVersion$ContentProfileConstraints. Values are defined\n            through local configuration.	hpdContentProfile	1.3.6.1.4.1.19376.1.2.4.1.27	0	0	\N	f	\N	1.3.6.1.4.1.1466.115.121.1.15
221	RFC2798: a JPEG image	jpegPhoto	0.9.2342.19200300.100.1.60	\N	\N	\N	f	\N	1.3.6.1.4.1.1466.115.121.1.28
222	RFC2798: personal identity information, a PKCS #12 PFX	userPKCS12	2.16.840.1.113730.3.1.216	\N	\N	\N	f	\N	1.3.6.1.4.1.1466.115.121.1.5
223	RFC2798: PKCS#7 SignedData used to support S/MIME	userSMIMECertificate	2.16.840.1.113730.3.1.40	\N	\N	\N	f	\N	1.3.6.1.4.1.1466.115.121.1.5
224	RFC2798: preferred name to be used when displaying entries	displayName	2.16.840.1.113730.3.1.241	0	\N	\N	t	0	1.3.6.1.4.1.1466.115.121.1.15
225	RFC2798: identifies a department within an organization	departmentNumber	2.16.840.1.113730.3.1.2	0	\N	\N	f	0	1.3.6.1.4.1.1466.115.121.1.15
226	RFC2798: preferred written or spoken language for a person	preferredLanguage	2.16.840.1.113730.3.1.39	0	\N	\N	t	0	1.3.6.1.4.1.1466.115.121.1.15
227	RFC2798: type of employment for a person	employeeType	2.16.840.1.113730.3.1.4	0	\N	\N	f	0	1.3.6.1.4.1.1466.115.121.1.15
228	RFC2798: numerically identifies an employee within an\n            organization	employeeNumber	2.16.840.1.113730.3.1.3	0	\N	\N	t	0	1.3.6.1.4.1.1466.115.121.1.15
229	RFC2798: vehicle license or registration plate	carLicense	2.16.840.1.113730.3.1.1	0	\N	\N	f	0	1.3.6.1.4.1.1466.115.121.1.15
230	RFC2256: enhanced search guide	enhancedSearchGuide	2.5.4.47	\N	\N	\N	f	\N	1.3.6.1.4.1.1466.115.121.1.21
231	RFC2256: descriptive information	description	2.5.4.13	0	\N	\N	f	0	1.3.6.1.4.1.1466.115.121.1.15
232	RFC2256: X.509 CA certificate, use ;binary	cACertificate	2.5.4.37	\N	\N	\N	f	\N	1.3.6.1.4.1.1466.115.121.1.8
233	RFC2256: business category	businessCategory	2.5.4.15	0	\N	\N	f	0	1.3.6.1.4.1.1466.115.121.1.15
234	RFC2256: owner (of the object)	owner	2.5.4.32	1	\N	\N	f	\N	1.3.6.1.4.1.1466.115.121.1.12
235	RFC1274: RFC822 Mailbox	mail	0.9.2342.19200300.100.1.3	\N	\N	\N	f	\N	1.3.6.1.4.1.1466.115.121.1.26
236	RFC1274: RFC822 Mailbox	rfc822Mailbox	0.9.2342.19200300.100.1.3	\N	\N	\N	f	\N	1.3.6.1.4.1.1466.115.121.1.26
237	RFC2256: X.509 cross certificate pair, use ;binary	crossCertificatePair	2.5.4.40	\N	\N	\N	f	\N	1.3.6.1.4.1.1466.115.121.1.10
238	RFC2256: DN qualifier	dnQualifier	2.5.4.46	0	\N	\N	f	0	1.3.6.1.4.1.1466.115.121.1.44
239	RFC2256: supported application context	supportedApplicationContext	2.5.4.30	5	\N	\N	f	\N	1.3.6.1.4.1.1466.115.121.1.38
240	RFC2256: occupant of role	roleOccupant	2.5.4.33	1	\N	\N	f	\N	1.3.6.1.4.1.1466.115.121.1.12
241	RFC2256: Post Office Box	postOfficeBox	2.5.4.18	0	\N	\N	f	0	1.3.6.1.4.1.1466.115.121.1.15
242	RFC2256: postal code	postalCode	2.5.4.17	0	\N	\N	f	0	1.3.6.1.4.1.1466.115.121.1.15
243	RFC2256: X.121 Address	x121Address	2.5.4.24	\N	\N	\N	f	\N	1.3.6.1.4.1.1466.115.121.1.36
244	RFC2256: street address of this object	street	2.5.4.9	0	\N	\N	f	0	1.3.6.1.4.1.1466.115.121.1.15
245	RFC2256: street address of this object	streetAddress	2.5.4.9	0	\N	\N	f	0	1.3.6.1.4.1.1466.115.121.1.15
246	RFC2256: first name(s) for which the entity is known by	givenName	2.5.4.42	0	\N	\N	f	0	1.3.6.1.4.1.1466.115.121.1.15
247	RFC2256: first name(s) for which the entity is known by	gn	2.5.4.42	0	\N	\N	f	0	1.3.6.1.4.1.1466.115.121.1.15
248	RFC2256: protocol information	protocolInformation	2.5.4.48	\N	\N	\N	f	\N	1.3.6.1.4.1.1466.115.121.1.42
249	RFC2256: serial number of the entity	serialNumber	2.5.4.5	0	\N	\N	f	0	1.3.6.1.4.1.1466.115.121.1.44
250	RFC2256: Telephone Number	telephoneNumber	2.5.4.20	4	\N	\N	f	2	1.3.6.1.4.1.1466.115.121.1.50
251	RFC2256: organization this object belongs to	o	2.5.4.10	0	\N	\N	f	0	1.3.6.1.4.1.1466.115.121.1.15
252	RFC2256: organization this object belongs to	organizationName	2.5.4.10	0	\N	\N	f	0	1.3.6.1.4.1.1466.115.121.1.15
253	RFC2256: Facsimile (Fax) Telephone Number	facsimileTelephoneNumber	2.5.4.23	\N	\N	\N	f	\N	1.3.6.1.4.1.1466.115.121.1.22
254	RFC2256: Facsimile (Fax) Telephone Number	fax	2.5.4.23	\N	\N	\N	f	\N	1.3.6.1.4.1.1466.115.121.1.22
255	RFC2256: ISO-3166 country 2-letter code	c	2.5.4.6	0	\N	\N	t	0	1.3.6.1.4.1.1466.115.121.1.15
256	RFC2256: ISO-3166 country 2-letter code	countryName	2.5.4.6	0	\N	\N	t	0	1.3.6.1.4.1.1466.115.121.1.15
257	RFC2256: unique member of a group	uniqueMember	2.5.4.50	\N	\N	\N	f	\N	1.3.6.1.4.1.1466.115.121.1.34
258	RFC2256: delta revocation list; use ;binary	deltaRevocationList	2.5.4.53	\N	\N	\N	f	\N	1.3.6.1.4.1.1466.115.121.1.9
259	RFC2256: Telex Number	telexNumber	2.5.4.21	\N	\N	\N	f	\N	1.3.6.1.4.1.1466.115.121.1.52
260	RFC2256: name qualifier indicating a generation	generationQualifier	2.5.4.44	0	\N	\N	f	0	1.3.6.1.4.1.1466.115.121.1.15
261	RFC2459: legacy attribute for email addresses in DNs	email	1.2.840.113549.1.9.1	\N	\N	\N	f	\N	1.3.6.1.4.1.1466.115.121.1.26
262	RFC2459: legacy attribute for email addresses in DNs	emailAddress	1.2.840.113549.1.9.1	\N	\N	\N	f	\N	1.3.6.1.4.1.1466.115.121.1.26
263	RFC2459: legacy attribute for email addresses in DNs	pkcs9email	1.2.840.113549.1.9.1	\N	\N	\N	f	\N	1.3.6.1.4.1.1466.115.121.1.26
264	RFC2256: initials of some or all of names, but not the\n            surname(s).	initials	2.5.4.43	0	\N	\N	f	0	1.3.6.1.4.1.1466.115.121.1.15
265	RFC2256: registered postal address	registeredAddress	2.5.4.26	\N	\N	\N	f	\N	1.3.6.1.4.1.1466.115.121.1.41
266	RFC2256: state or province which this object resides in	st	2.5.4.8	0	\N	\N	f	0	1.3.6.1.4.1.1466.115.121.1.15
267	RFC2256: state or province which this object resides in	stateOrProvinceName	2.5.4.8	0	\N	\N	f	0	1.3.6.1.4.1.1466.115.121.1.15
268	RFC2256: postal address	postalAddress	2.5.4.16	\N	\N	\N	f	\N	1.3.6.1.4.1.1466.115.121.1.41
269	RFC2256: X.509 user certificate, use ;binary	userCertificate	2.5.4.36	\N	\N	\N	f	\N	1.3.6.1.4.1.1466.115.121.1.8
270	RFC2256: last (family) name(s) for which the entity is known by	sn	2.5.4.4	0	\N	\N	t	0	1.3.6.1.4.1.1466.115.121.1.15
271	RFC2256: last (family) name(s) for which the entity is known by	surname	2.5.4.4	0	\N	\N	t	0	1.3.6.1.4.1.1466.115.121.1.15
272	RFC2256: title associated with the entity	title	2.5.4.12	0	\N	\N	f	0	1.3.6.1.4.1.1466.115.121.1.15
273	RFC2256: presentation address	presentationAddress	2.5.4.29	\N	\N	\N	t	\N	1.3.6.1.4.1.1466.115.121.1.43
274	RFC2256: X.509 certificate revocation list, use ;binary	certificateRevocationList	2.5.4.39	\N	\N	\N	f	\N	1.3.6.1.4.1.1466.115.121.1.9
275	RFC2256: supported algorithms	supportedAlgorithms	2.5.4.52	\N	\N	\N	f	\N	1.3.6.1.4.1.1466.115.121.1.49
276	RFC2256: preferred delivery method	preferredDeliveryMethod	2.5.4.28	\N	\N	\N	t	\N	1.3.6.1.4.1.1466.115.121.1.14
277	RFC2256: locality which this object resides in	l	2.5.4.7	0	\N	\N	f	0	1.3.6.1.4.1.1466.115.121.1.15
278	RFC2256: locality which this object resides in	localityName	2.5.4.7	0	\N	\N	f	0	1.3.6.1.4.1.1466.115.121.1.15
279	RFC2256: name of DMD	dmdName	2.5.4.54	0	\N	\N	f	0	1.3.6.1.4.1.1466.115.121.1.15
280	RFC1274: user identifier	uid	0.9.2342.19200300.100.1.1	0	\N	\N	f	0	1.3.6.1.4.1.1466.115.121.1.15
281	RFC1274: user identifier	userid	0.9.2342.19200300.100.1.1	0	\N	\N	f	0	1.3.6.1.4.1.1466.115.121.1.15
282	RFC2256: Physical Delivery Office Name	physicalDeliveryOfficeName	2.5.4.19	0	\N	\N	f	0	1.3.6.1.4.1.1466.115.121.1.15
283	RFC2256: house identifier	houseIdentifier	2.5.4.51	0	\N	\N	f	0	1.3.6.1.4.1.1466.115.121.1.15
284	RFC2256: Teletex Terminal Identifier	teletexTerminalIdentifier	2.5.4.22	\N	\N	\N	f	\N	1.3.6.1.4.1.1466.115.121.1.51
285	RFC2256: DN of related object	seeAlso	2.5.4.34	1	\N	\N	f	\N	1.3.6.1.4.1.1466.115.121.1.12
286	RFC2256: member of a group	member	2.5.4.31	1	\N	\N	f	\N	1.3.6.1.4.1.1466.115.121.1.12
287	RFC2256: destination indicator	destinationIndicator	2.5.4.27	0	\N	\N	f	0	1.3.6.1.4.1.1466.115.121.1.44
288	RFC2256: international ISDN number	internationaliSDNNumber	2.5.4.25	\N	\N	\N	f	\N	1.3.6.1.4.1.1466.115.121.1.36
289	RFC2256: organizational unit this object belongs to	ou	2.5.4.11	0	\N	\N	f	0	1.3.6.1.4.1.1466.115.121.1.15
290	RFC2256: organizational unit this object belongs to	organizationalUnitName	2.5.4.11	0	\N	\N	f	0	1.3.6.1.4.1.1466.115.121.1.15
291	RFC2256: X.500 unique identifier	x500UniqueIdentifier	2.5.4.45	\N	\N	\N	f	\N	1.3.6.1.4.1.1466.115.121.1.6
292	RFC1274: domain associated with object	associatedDomain	0.9.2342.19200300.100.1.37	\N	\N	\N	f	\N	1.3.6.1.4.1.1466.115.121.1.26
293	RFC2256: search guide, obsoleted by enhancedSearchGuide	searchGuide	2.5.4.14	\N	\N	\N	f	\N	1.3.6.1.4.1.1466.115.121.1.25
294	RFC2256: X.509 authority revocation list, use ;binary	authorityRevocationList	2.5.4.38	\N	\N	\N	f	\N	1.3.6.1.4.1.1466.115.121.1.9
295	RFC1274/2247: domain component	dc	0.9.2342.19200300.100.1.25	\N	\N	\N	t	\N	1.3.6.1.4.1.1466.115.121.1.26
296	RFC1274/2247: domain component	domainComponent	0.9.2342.19200300.100.1.25	\N	\N	\N	t	\N	1.3.6.1.4.1.1466.115.121.1.26
297	RFC2256: knowledge information	knowledgeInformation	2.5.4.2	0	\N	\N	f	\N	1.3.6.1.4.1.1466.115.121.1.15
366	Public key and certificate for the users non- repudiation signing certificate\n            used for health transactions	HcSigningCertificate	1.0.21091.2.0.3	\N	0	\N	f	\N	1.3.6.1.4.1.1466.115.121.1.5
367	Date on which the reference vocabulary is/was effective	HcReferenceEffectiveDate	1.0.21091.2.10.2	2	0	\N	t	1	1.3.6.1.4.1.1466.115.121.1.24
368	Used for credentials, power of attorney, health care decision maker, etc.\n            Populated with P7 formatted certificate	HcAttributeCertificate	1.0.21091.2.0.4	\N	0	\N	f	\N	1.3.6.1.4.1.1466.115.121.1.5
369	(Issuing authority: Code system: Code) Populate with HL-7 coding for\n            roles	HcRole	1.0.21091.2.0.5	0	0	\N	f	0	1.3.6.1.4.1.1466.115.121.1.15
370	HcDeviceDateRecalled	HcDeviceDateRecalled	1.0.21091.2.11.3	2	0	0	t	\N	1.3.6.1.4.1.1466.115.121.1.24
371	Text representation of the user profession (issuing authority: Code System:\n            Code)	HcProfession	1.0.21091.2.2.1	0	0	\N	f	0	1.3.6.1.4.1.1466.115.121.1.15
372	Use DN of the organization	HcPracticeLocation	1.0.21091.2.2.4	1	0	\N	f	\N	1.3.6.1.4.1.1466.115.121.1.12
373	Date on which the device was retrieved	HcDeviceDateRetrieved	1.0.21091.2.11.4	2	0	0	t	\N	1.3.6.1.4.1.1466.115.121.1.24
374	(Issuer:Number) Tracking number assigned to the device	HcDeviceTrackingNumber	1.0.21091.2.11.6	0	0	\N	f	0	1.3.6.1.4.1.1466.115.121.1.15
375	Date of closure of the organization or date when the organization changed\n            name/affiliation	HcClosureDate	1.0.21091.2.0.10	2	0	0	t	\N	1.3.6.1.4.1.1466.115.121.1.24
376	Date on which the device was issued to the recipient	HcDeviceDateOfIssue	1.0.21091.2.11.2	2	0	0	t	\N	1.3.6.1.4.1.1466.115.121.1.24
377	Device certificates issued	HcDeviceCertificate	1.0.21091.2.11.5	\N	0	\N	f	\N	1.3.6.1.4.1.1466.115.121.1.5
378	Date on which the reference vocabulary was issued	HcReferenceDateOfIssue	1.0.21091.2.10.5	2	0	0	t	\N	1.3.6.1.4.1.1466.115.121.1.24
379	Date on which the reference vocabulary is/was invalid	HcReferenceInvalidDate	1.0.21091.2.10.6	2	0	0	t	\N	1.3.6.1.4.1.1466.115.121.1.24
380	The address as registered with the regulatory authority. This shall be\n            structured the same as PostalAddress	HcRegisteredAddr	1.0.21091.2.4.2	0	0	\N	f	0	1.3.6.1.4.1.1466.115.121.1.15
381	Version number of the coded reference	HcReferenceVersion	1.0.21091.2.10.7	0	0	\N	f	0	1.3.6.1.4.1.1466.115.121.1.15
382	The legal name of the entity as registered with the health care regulating\n            authority	HcRegisteredName	1.0.21091.2.4.1	0	0	\N	f	0	1.3.6.1.4.1.1466.115.121.1.15
383	The HL-7 defined sex	HL7Sex	1.0.21091.2.1.7	0	0	\N	f	0	1.3.6.1.4.1.1466.115.121.1.15
384	The HL-7 defined mothers maiden name	HL7MothersMaidenName	1.0.21091.2.1.5	0	0	\N	f	0	1.3.6.1.4.1.1466.115.121.1.15
385	Location of the Master Patient Index Service(s) available to identify patient\n            clinical records	HcMPILocation	1.0.21091.2.1.2	0	0	\N	f	0	1.3.6.1.4.1.1466.115.121.1.15
386	The HL-7 defined religion. While this attribute is important for spiritual\n            aspects of patient care, it must be managed with the utmost protection as this can be\n            sensitive private information	HL7Religion	1.0.21091.2.1.16	0	0	\N	f	0	1.3.6.1.4.1.1466.115.121.1.15
387	concatenated value:Reference code:Description	HcReferenceDescription	1.0.21091.2.10.3	0	0	\N	f	0	1.3.6.1.4.1.1466.115.121.1.15
388	DN of successor entry	HcSuccessorName	1.0.21091.2.0.11	1	0	\N	f	\N	1.3.6.1.4.1.1466.115.121.1.12
389	The entry for the individual responsible for EDI administration	EdiAdministrativeContact	1.0.21091.2.0.7	1	0	\N	f	\N	1.3.6.1.4.1.1466.115.121.1.12
390	DN of person or HCOrganizationalRole responsible for this entry (medical\n            staffing, legal review, contract staff, employee)	HcResponsibleParty	1.0.21091.2.7.1	1	0	\N	f	\N	1.3.6.1.4.1.1466.115.121.1.12
391	Record entry of person(s) able to sign/act on behalf of the\n            subject	HcSubstituteDecisionMaker	1.0.21091.2.1.3	1	0	\N	f	\N	1.3.6.1.4.1.1466.115.121.1.12
392	The HL-7 defined patient alias	HL7PatientAlias	1.0.21091.2.1.8	0	0	\N	f	0	1.3.6.1.4.1.1466.115.121.1.15
393	Location of service(s) offering biometric or other identification verification\n            service	HcIdentificationService	1.0.21091.2.0.2	0	0	\N	f	0	1.3.6.1.4.1.1466.115.121.1.15
394	Phone number assigned to the device (i.e. PDA)	HcDevicePhone	1.0.21091.2.11.7	4	0	\N	f	2	1.3.6.1.4.1.1466.115.121.1.50
395	DN of the individual to whom the device has been issued	HcDeviceIssuedTo	1.0.21091.2.11.1	1	0	\N	t	\N	1.3.6.1.4.1.1466.115.121.1.12
396	Used for storing health care organization certificates	HcOrganizationCertificates	1.0.21091.2.0.9	\N	0	\N	f	\N	1.3.6.1.4.1.1466.115.121.1.5
397	Location restrictions from where the role is valid (i.e. from the emergency\n            department only, from IP address, etc.)	HcRoleLocationRestriction	1.0.21091.2.0.13	0	0	\N	f	0	1.3.6.1.4.1.1466.115.121.1.15
398	Use DN of the organization	HcPrincipalPracticeLocation	1.0.21091.2.2.3	1	0	\N	t	\N	1.3.6.1.4.1.1466.115.121.1.12
399	Times in GMT format that the user may act under this role	HcRoleValidTime	1.0.21091.2.0.12	0	0	\N	f	0	1.3.6.1.4.1.1466.115.121.1.15
400	(Issuing authority: Code system: Code) Populate with HL-7 coding for\n            specialization	HcSpecialization	1.0.21091.2.0.6	0	0	\N	f	0	1.3.6.1.4.1.1466.115.121.1.15
401	The hl-7 defined date and time of birth	HL7DateTimeofBirth	1.0.21091.2.1.6	2	0	0	f	\N	1.3.6.1.4.1.1466.115.121.1.24
402	This may be an identifiable, anonymous, or pseudonymous identifier. (Issuing\n            authority:Type: ID)	HcConsumerID	1.0.21091.2.1.1	0	0	\N	f	0	1.3.6.1.4.1.1466.115.121.1.15
403	Name of assigning authority:payers plan:ID	HcPayerProductID	1.0.21091.2.5.1	0	0	\N	f	0	1.3.6.1.4.1.1466.115.121.1.15
404	Used to indicate organization DN of organization	HcOrganization	1.0.21091.2.3.1	1	0	\N	f	\N	1.3.6.1.4.1.1466.115.121.1.12
405	Health care organizations where health care services are rendered	HcServiceLocations	1.0.21091.2.4.3	1	0	\N	f	\N	1.3.6.1.4.1.1466.115.121.1.12
406	HPD Organization ID	hpdOrgId	1.3.6.1.4.1.19376.1.2.4.3.100	\N	\N	\N	t	\N	\N
407	The HL-7 defined place of birth	HL7BirthPlace	1.0.21091.2.1.22	0	0	\N	f	0	1.3.6.1.4.1.1466.115.121.1.15
408	OID of the health care vocabulary used	HcVocabularyOID	1.0.21091.2.10.4	5	0	\N	t	\N	1.3.6.1.4.1.1466.115.121.1.38
409	Authority responsible for coding scheme	HcIssuingAuthority	1.0.21091.2.10.1	0	0	\N	f	0	1.3.6.1.4.1.1466.115.121.1.15
410	The health care identifier. Where this is a regulated health care professional,\n            this must minimally contain an entry indicating the identifier assigned by the\n            regulating authority (issuing authority:Type:ID:- status)	HcIdentifier	1.0.21091.2.0.1	0	0	\N	t	0	1.3.6.1.4.1.1466.115.121.1.15
411	The entry for the individual to contact with clinical issues	ClinicalInformationContact	1.0.21091.2.0.8	1	0	\N	f	\N	1.3.6.1.4.1.1466.115.121.1.12
412	The HL-7 defined county code	HL7CountyCode	1.0.21091.2.1.11	0	0	\N	f	0	1.3.6.1.4.1.1466.115.121.1.15
413	The HL-7 defined patient death date and time	HL7PatientDeathDateandTime	1.0.21091.2.1.28	2	0	0	t	\N	1.3.6.1.4.1.1466.115.121.1.24
\.


--
-- Name: hpd_ldap_attribute_types hpd_ldap_attribute_types_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY hpd_ldap_attribute_types
    ADD CONSTRAINT hpd_ldap_attribute_types_pkey PRIMARY KEY (id);


--
-- Name: hpd_ldap_attribute_types uk_xpm0rgr3crhwjvqndqj4i0; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY hpd_ldap_attribute_types
    ADD CONSTRAINT uk_xpm0rgr3crhwjvqndqj4i0 UNIQUE (name);


--
-- PostgreSQL database dump complete
--

