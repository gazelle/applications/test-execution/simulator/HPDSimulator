CREATE SEQUENCE hpd_ldap_attribute_types_id_seq INCREMENT BY 1 MINVALUE 0 START 0;
CREATE SEQUENCE hpd_ldap_object_classes_id_seq INCREMENT BY 1 MINVALUE 0 START 0;

ALTER TABLE hpd_ldap_attribute_types ADD COLUMN id INTEGER UNIQUE;
UPDATE hpd_ldap_attribute_types SET id = nextval('hpd_ldap_attribute_types_id_seq');
ALTER TABLE hpd_ldap_attribute_types DROP CONSTRAINT hpd_ldap_attribute_types_pkey;
ALTER TABLE public.hpd_ldap_attribute_types ADD CONSTRAINT hpd_ldap_attribute_types_id_pk PRIMARY KEY (id);

ALTER TABLE hpd_ldap_object_classes ADD COLUMN id INTEGER UNIQUE;
UPDATE hpd_ldap_object_classes SET id = nextval('hpd_ldap_object_classes_id_seq');
ALTER TABLE hpd_ldap_object_classes RENAME COLUMN supobjectclass TO sup_object_class;
ALTER TABLE hpd_ldap_object_classes RENAME COLUMN typeobjectclass TO type_object_class;

ALTER TABLE hpd_ldap_object_classes_may RENAME COLUMN element TO may;
ALTER TABLE hpd_ldap_object_classes_may ADD COLUMN object_classes_id INTEGER;
UPDATE hpd_ldap_object_classes_may SET object_classes_id = (select id from hpd_ldap_object_classes where name = hpd_ldap_object_classes_may.hpd_ldap_object_classes_name);
ALTER TABLE hpd_ldap_object_classes_may  ADD CONSTRAINT hpd_ldap_object_classes_may_fk FOREIGN KEY (object_classes_id) REFERENCES hpd_ldap_object_classes (id);
ALTER TABLE hpd_ldap_object_classes_may DROP COLUMN hpd_ldap_object_classes_name;

ALTER TABLE hpd_ldap_object_classes_must RENAME COLUMN element TO must;
ALTER TABLE hpd_ldap_object_classes_must ADD COLUMN object_classes_id INTEGER;
UPDATE hpd_ldap_object_classes_must SET object_classes_id = (select id from hpd_ldap_object_classes where name = hpd_ldap_object_classes_must.hpd_ldap_object_classes_name);
ALTER TABLE hpd_ldap_object_classes_must  ADD CONSTRAINT hpd_ldap_object_classes_must_fk FOREIGN KEY (object_classes_id) REFERENCES hpd_ldap_object_classes (id);
ALTER TABLE hpd_ldap_object_classes_must DROP COLUMN hpd_ldap_object_classes_name;

ALTER TABLE ldap_node_object_class RENAME COLUMN object_class_id TO object_class_id_old;
ALTER TABLE ldap_node_object_class ADD COLUMN object_class_id INTEGER;
UPDATE ldap_node_object_class SET object_class_id = (SELECT id from hpd_ldap_object_classes WHERE name = ldap_node_object_class.object_class_id_old);
ALTER TABLE public.ldap_node_object_class  ADD CONSTRAINT ldap_node_object_class_fk FOREIGN KEY (object_class_id) REFERENCES hpd_ldap_object_classes (id);
ALTER TABLE ldap_node_object_class DROP CONSTRAINT fk54619cfd98a4c9a4;
--ALTER TABLE public.hpd_ldap_object_classes DROP CONSTRAINT hpd_ldap_object_classes_pkey;
ALTER TABLE public.ldap_node_object_class DROP CONSTRAINT ldap_node_object_class_ldap_node_id_object_class_id_key;
ALTER TABLE public.ldap_node_object_class
  ADD CONSTRAINT ldap_node_object_class_ldap_node_id_object_class_id_key UNIQUE (ldap_node_id, object_class_id);
ALTER TABLE ldap_node_object_class DROP COLUMN object_class_id_old;
--ALTER TABLE public.hpd_ldap_object_classes DROP CONSTRAINT hpd_ldap_object_classes_pkey;
--ALTER TABLE public.hpd_ldap_object_classes DROP CONSTRAINT uk_gcuvfhoc1fbgpgmyetttimyom;
ALTER TABLE public.hpd_ldap_object_classes DROP CONSTRAINT hpd_ldap_object_classes_pkey;
ALTER TABLE public.hpd_ldap_object_classes ADD CONSTRAINT hpd_ldap_object_classes_pkey PRIMARY KEY (id);