package net.ihe.gazelle.chhpd.simulator.providerinfoconsumer;

import net.ihe.gazelle.chhpd.pidd.model.DownloadRequest;
import net.ihe.gazelle.hpd.AuthRequest;
import net.ihe.gazelle.hpd.Control;
import net.ihe.gazelle.hpd.simulator.action.AbstractInitiator;
import net.ihe.gazelle.hpd.simulator.model.ProviderInformationDirectorySUTConfiguration;
import net.ihe.gazelle.hpd.simulator.model.ProviderInformationDirectorySUTConfigurationQuery;
import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.hql.restrictions.HQLRestrictions;
import net.ihe.gazelle.simulator.common.tf.model.Actor;
import net.ihe.gazelle.simulator.common.tf.model.Transaction;
import net.ihe.gazelle.simulator.message.model.TransactionInstance;
import net.ihe.gazelle.simulator.sut.model.SystemConfiguration;
import net.ihe.gazelle.ssov7.authn.domain.GazelleIdentity;
import net.ihe.gazelle.ssov7.authn.interlay.adapter.GazelleIdentityImpl;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Create;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Name("piddConsumer")
@Scope(ScopeType.PAGE)
public class PIDDConsumer extends AbstractInitiator implements Serializable {

    private final GazelleIdentity gazelleIdentity = GazelleIdentityImpl.instance();

    private DownloadRequest downloadRequest;
    private static final Logger LOG = LoggerFactory.getLogger(PIDDConsumer.class);

    @Create
    public void init() {
        setSelectedTransaction(Transaction.GetTransactionByKeyword("PIDD"));
        setSimulatedActor(Actor.findActorWithKeyword("PROV_INFO_CONS"));
        setDownloadRequest(new DownloadRequest());
    }

    @Override
    public List<ProviderInformationDirectorySUTConfiguration> listAvailableSut() {
        ProviderInformationDirectorySUTConfigurationQuery query = new ProviderInformationDirectorySUTConfigurationQuery();
        if (!gazelleIdentity.isLoggedIn()) {
            query.isPublic().eq(true);
        } else if (!gazelleIdentity.hasRole("admin_role")) {
            query.addRestriction(HQLRestrictions.or(HQLRestrictions.eq("isPublic", true),
                    HQLRestrictions.eq("owner", gazelleIdentity.getUsername())));
        }
        return query.getList();
    }

    public void addAuthRequest() {
        getDownloadRequest().setAuthRequest(new AuthRequest());
    }

    public void deleteAuthRequest() {
        getDownloadRequest().setAuthRequest(null);
    }

    public void addControl() {
        getDownloadRequest().authRequest.addControl(new Control());
    }

    public void removeControl(Control control) {
        getDownloadRequest().authRequest.removeControl(control);
    }


    public List<TransactionInstance> sendMessageToSut(SystemConfiguration sut, DownloadRequest downloadRequest) {
        if (sut == null) {
            FacesMessages.instance().add(StatusMessage.Severity.ERROR, "You must first select the system under test to use");
            return null;
        } else {
            try {
                PIDDSender sender = new PIDDSender(sut);
                sender.sendDownloadRequest(downloadRequest);
                TransactionInstance message = sender.getTransactionInstance();
                if (gazelleIdentity.isLoggedIn()) {
                    String company = gazelleIdentity.getOrganisationKeyword();
                    message.setCompanyKeyword(company);
                }
                message = message.save(EntityManagerService.provideEntityManager());
                List<TransactionInstance> instances = new ArrayList<TransactionInstance>();
                instances.add(message);
                return instances;
            } catch (Exception e) {
                FacesMessages.instance().add(StatusMessage.Severity.ERROR, "An error occurred when sending message: " + e.getMessage());
                LOG.error("Unexpected error while sending message: " + e.getMessage());
            }
            return null;
        }
    }

    @Override
    public void sendMessage() {
        setMessages(sendMessageToSut(getSelectedSut(), getDownloadRequest()));
    }

    @Override
    public void performAnotherTest() {
        setMessages(null);
        this.downloadRequest = new DownloadRequest();
    }

    public DownloadRequest getDownloadRequest() {
        if (this.downloadRequest == null) {
            this.downloadRequest = new DownloadRequest();
        }
        return this.downloadRequest;
    }


    public void setDownloadRequest(DownloadRequest downloadRequest) {
        this.downloadRequest = downloadRequest;
    }

}
