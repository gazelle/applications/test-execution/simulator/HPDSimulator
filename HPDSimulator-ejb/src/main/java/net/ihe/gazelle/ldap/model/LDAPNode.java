package net.ihe.gazelle.ldap.model;

import org.hibernate.annotations.Type;
import org.jboss.seam.Component;
import org.jboss.seam.annotations.Name;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

@Name("ldapNode")
@Entity
@Table(name = "ldap_node", schema = "public", uniqueConstraints = @UniqueConstraint(columnNames = "name"))
@SequenceGenerator(name = "ldap_node_sequence", sequenceName = "ldap_node_id_seq", allocationSize = 1)
public class LDAPNode implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 2354380561405401785L;

    @Id
    @Column(name = "id")
    @NotNull
    @GeneratedValue(generator = "ldap_node_sequence", strategy = GenerationType.SEQUENCE)
    private Integer id;

    @Column(name = "name")
    @NotNull
    private String name;

    @Column(name = "description")
    @Lob
    @Type(type = "text")
    private String description;

    @ManyToMany
    @JoinTable(name = "ldap_node_object_class", joinColumns = @JoinColumn(name = "ldap_node_id"), inverseJoinColumns = @JoinColumn(name = "object_class_id"), uniqueConstraints = @UniqueConstraint(columnNames = {
            "ldap_node_id", "object_class_id"}))
    private List<ObjectClasses> objectClasses;

    public LDAPNode() {

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<ObjectClasses> getObjectClasses() {
        if (objectClasses != null) {
            Collections.sort(objectClasses, new Comparator<ObjectClasses>() {
                @Override
                public int compare(ObjectClasses objectClasses, ObjectClasses objectClasses2) {
                    String o1 = objectClasses.getName().trim().toLowerCase();
                    String o2 = objectClasses2.getName().trim().toLowerCase();
                    return o1.compareTo(o2);
                }
            });
        }
        return objectClasses;
    }

    public void setObjectClasses(List<ObjectClasses> objectClasses) {
        this.objectClasses = objectClasses;
    }

    public Integer getId() {
        return id;
    }

    public LDAPNode save() {
        EntityManager entityManager = (EntityManager) Component.getInstance("entityManager");
        LDAPNode node = entityManager.merge(this);
        entityManager.flush();
        return node;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = (prime * result) + ((name == null) ? 0 : name.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        LDAPNode other = (LDAPNode) obj;
        if (name == null) {
            if (other.name != null) {
                return false;
            }
        } else if (!name.equals(other.name)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return this.name;
    }
}
