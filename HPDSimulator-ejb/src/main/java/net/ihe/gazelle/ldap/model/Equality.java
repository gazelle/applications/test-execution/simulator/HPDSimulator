/**
 * Equality.java
 *
 * File generated from the LDAStructure::Equality uml Enumeration
 * Generated by IHE - europe, gazelle team
 */
package net.ihe.gazelle.ldap.model;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * Description of the enumeration Equality.
 * 
 */

@XmlType(name = "Equality")
@XmlEnum
@XmlRootElement(name = "Equality")
public enum Equality {
	@XmlEnumValue("caseIgnoreMatch")
	CASEIGNOREMATCH("caseIgnoreMatch"),
	@XmlEnumValue("distinguishedNameMatch")
	DISTINGUISHEDNAMEMATCH("distinguishedNameMatch"),
	@XmlEnumValue("generalizedTimeMatch")
	GENERALIZEDTIMEMATCH("generalizedTimeMatch"),
	@XmlEnumValue("octetStringMatch")
	OCTETSTRINGMATCH("octetStringMatch"),
	@XmlEnumValue("telephoneNumberMatch")
	TELEPHONENUMBERMATCH("telephoneNumberMatch"),
	@XmlEnumValue("objectIdentifierMatch")
	OBJECTIDENTIFIERMATCH("objectIdentifierMatch");

	private final String value;

	Equality(String v) {
		value = v;
	}

	public String value() {
		return value;
	}

	public static Equality fromValue(String v) {
		for (Equality c : Equality.values()) {
			if (c.value.equals(v)) {
				return c;
			}
		}
		throw new IllegalArgumentException(v);
	}

}