package net.ihe.gazelle.chcpi.action;

import net.ihe.gazelle.chcpi.model.CIDDMessage;
import net.ihe.gazelle.hpd.BatchRequest;
import net.ihe.gazelle.hpd.simulator.action.RequestType;
import net.ihe.gazelle.hpd.utils.SAXParserFactoryProvider;
import net.ihe.gazelle.hpdTransformer.HPDTransformer;
import net.ihe.gazelle.validation.DocumentValidXSD;
import net.ihe.gazelle.validator.hpd.util.XMLValidation;
import org.jboss.seam.Component;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Create;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManager;
import javax.xml.bind.JAXBException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.nio.charset.StandardCharsets;
import java.util.List;

@Name("ciddMessageManager")
@Scope(ScopeType.PAGE)
public class CIDDMessageManager {

    private boolean displayList;
    private boolean editMessage;
    private List<CIDDMessage> ciddMessages;
    private CIDDMessage selectedCiddMessage;

    private static Logger log = LoggerFactory.getLogger(CIDDMessageManager.class);

    @Create
    public void listCIDDMessages() {
        ciddMessages = CIDDMessageDAO.queryAllCIDDMessages();
        this.displayList = true;
        this.editMessage = false;
    }

    public void editCiddMessage(CIDDMessage ciddMessage) {
        if (ciddMessage.getId() != null) {
            EntityManager entityManager = (EntityManager) Component.getInstance("entityManager");
            this.selectedCiddMessage = entityManager.find(CIDDMessage.class, ciddMessage.getId());
        } else {
            this.selectedCiddMessage = ciddMessage;
        }
        this.editMessage = true;
        this.displayList = false;
    }

    public void createCiddMessage() {
        editCiddMessage(new CIDDMessage());
    }

    public void cancelEdition() {
        this.displayList = true;
        this.editMessage = false;
        this.selectedCiddMessage = null;
    }

    public void saveMessage() {
        if (selectedCiddMessage != null) {
            prettyPrintMessage(selectedCiddMessage);
            Boolean validation = validate(selectedCiddMessage);
            listCIDDMessages();
            if (validation == null || !validation) {
                FacesMessages.instance().add(StatusMessage.Severity.ERROR, "The message is not valid (XSD validation)");
            }
        }
    }

    public void deleteMessage(CIDDMessage ciddMessage) {
        CIDDMessageDAO.delete(ciddMessage);
        listCIDDMessages();
    }

    public void deleteSelectedMessage() {
        CIDDMessageDAO.delete(selectedCiddMessage);
        listCIDDMessages();
    }

    public void validateAll() {
        for (CIDDMessage message : getCiddMessages()) {
            validate(message);
        }
    }

    public boolean validate(CIDDMessage message) {
        message.setValid(validateWithoutSaving(message));
        CIDDMessageDAO.save(message);
        return message.getValid();
    }

    protected boolean validateWithoutSaving(CIDDMessage message) {
        XMLValidation xmlValidation = new XMLValidation(SAXParserFactoryProvider.getHpdFactory(),
                SAXParserFactoryProvider.getXsd());
        DocumentValidXSD documentValidXSD = xmlValidation.isXSDValid(message.getMessage());
        boolean valid = true;
        if (documentValidXSD != null && documentValidXSD.getResult() != null) {
            if (documentValidXSD.getResult().equals("PASSED")) {
                try {
                    InputStream is = new ByteArrayInputStream(message.getMessage().getBytes(StandardCharsets.UTF_8));
                    BatchRequest batchRequest = HPDTransformer.unmarshallMessage(BatchRequest.class, is);
                    // Only addRequest, modifyRequest, modDNRequest and delRequest are allowed
                    if (!batchRequest.getSearchRequest().isEmpty() ||
                            batchRequest.getAuthRequest() != null ||
                            !batchRequest.getAbandonRequest().isEmpty() ||
                            !batchRequest.getCompareRequest().isEmpty() ||
                            !batchRequest.getExtendedRequest().isEmpty()) {
                        log.info("CIDD message " + message.getName() + " contains not allowed element");
                        valid = false;
                    }
                } catch (JAXBException e) {
                    valid = false;
                    log.error("Cannot unmarshall CIDD message " + message.getName() + " : " + e.getMessage());
                }
            } else {
                valid = false;
            }
        }
        return valid;
    }

    public static void prettyPrintMessage(CIDDMessage message) {
        try {
            String xml = message.getMessage();

            // Setup pretty print options
            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            ;
            Transformer serializer = transformerFactory.newTransformer();
            //Setup indenting to "pretty print"
            serializer.setOutputProperty(OutputKeys.INDENT, "yes");
            serializer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "no");
            serializer.setOutputProperty(OutputKeys.METHOD, "xml");
            serializer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "4");
            serializer.setOutputProperty("{http://xml.apache.org/xslt}indent-number", "1");

            // Return pretty print xml string
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            serializer.transform(new StreamSource(new StringReader(xml)), new StreamResult(bos));
            bos.close();
            message.setMessage(new String(bos.toByteArray(), StandardCharsets.UTF_8));
        } catch (TransformerException | IOException e) {
            log.error("Error pretty printing the message " + message.getName() + " : " + e.getMessage());
        }
    }

    public RequestType[] getMessageTypes() {
        return RequestType.values();
    }

    public boolean isDisplayList() {
        return displayList;
    }

    public void setDisplayList(boolean displayList) {
        this.displayList = displayList;
    }

    public boolean isEditMessage() {
        return editMessage;
    }

    public void setEditMessage(boolean editMessage) {
        this.editMessage = editMessage;
    }

    public List<CIDDMessage> getCiddMessages() {
        return ciddMessages;
    }

    public void setCiddMessages(List<CIDDMessage> ciddMessages) {
        this.ciddMessages = ciddMessages;
    }

    public CIDDMessage getSelectedCiddMessage() {
        return selectedCiddMessage;
    }

    public void setSelectedCiddMessage(CIDDMessage selectedCiddMessage) {
        this.selectedCiddMessage = selectedCiddMessage;
    }
}
