package net.ihe.gazelle.chcpi.simulator.cpiprovider;

import net.ihe.gazelle.chcpi.model.DownloadRequest;
import net.ihe.gazelle.chcpi.model.DownloadResponse;
import net.ihe.gazelle.common.interfacegenerator.GenerateInterface;
import net.ihe.gazelle.hpd.BatchRequest;
import net.ihe.gazelle.hpd.BatchResponse;
import net.ihe.gazelle.hpd.utils.HPDSoapConstants;
import net.ihe.gazelle.simulator.common.tf.model.Actor;
import net.ihe.gazelle.simulator.common.tf.model.Transaction;
import org.apache.commons.cli.MissingArgumentException;
import org.jboss.seam.annotations.Name;

import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.jws.HandlerChain;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.servlet.http.HttpServletRequest;
import javax.xml.ws.Action;
import javax.xml.ws.BindingType;
import javax.xml.ws.RespectBinding;
import javax.xml.ws.WebServiceContext;
import javax.xml.ws.handler.MessageContext;
import javax.xml.ws.soap.Addressing;

@Stateless
@Name("cpiProviderService")
@WebService(portName = HPDSoapConstants.CHCPI_PORT_NAME, name = HPDSoapConstants.CHCPI_PORT_TYPE, targetNamespace =
        HPDSoapConstants.CHCPI_TARGET_NAMESPACE, serviceName = HPDSoapConstants.CHCPI_SERVICE_NAME)
// Unwrap parameters
@SOAPBinding(parameterStyle = SOAPBinding.ParameterStyle.BARE)
// Adds header in response and allows server to process mustUnderstand
@Addressing(enabled = true, required = true)
// Force SOAP 1.2
@BindingType(javax.xml.ws.soap.SOAPBinding.SOAP12HTTP_BINDING)
@RespectBinding(enabled = true)
@GenerateInterface(value = "CpiProviderServiceRemote", isLocal = false, isRemote = true)
@HandlerChain(file = "handler-chain.xml")
public class CPIProviderService implements CpiProviderServiceRemote {

    // SOAP Context
    @Resource
    private WebServiceContext context;

    /**
     * CH:CPI CH:CIQ
     *
     * @param request : batchRequest received from the SUT
     * @return the batchResponse
     */
    @Override
    @WebMethod(operationName = "CommunityQueryRequest", action = HPDSoapConstants.CHCPI_COMMUNITY_QUERY_ACTION)
    @WebResult(name = "batchResponse", partName = "body", targetNamespace = HPDSoapConstants.DSML_NAMESPACE)
    @Action(input = HPDSoapConstants.CHCPI_COMMUNITY_QUERY_ACTION, output = HPDSoapConstants.CHCPI_COMMUNITY_QUERY_RESPONSE_ACTION)
    public BatchResponse CommunityQueryRequest(
            @WebParam(name = "batchRequest", partName = "body", targetNamespace = HPDSoapConstants.DSML_NAMESPACE) BatchRequest request) throws SchemaValidationException {
        CPIProvider directory = new CPIProvider(
                Transaction.GetTransactionByKeyword("CIQ"), Actor.findActorWithKeyword("CPI_CONS"), getRemoteHostFromContext());
        return directory.handleCPIRequest(request);
    }

    @WebMethod(operationName = "CommunityDownloadRequestMessage", action = HPDSoapConstants.CHCPI_COMMUNITY_DOWNLOAD_ACTION)
    @WebResult(name = "downloadResponse", partName = "body", targetNamespace = HPDSoapConstants.CHCPI_NAMESPACE)
    @Action(input = HPDSoapConstants.CHCPI_COMMUNITY_DOWNLOAD_ACTION, output = HPDSoapConstants.CHCPI_COMMUNITY_DOWNLOAD_ACTION_RESPONSE)
    public DownloadResponse CommunityDownloadRequestMessage(
            @WebParam(name = "downloadRequest", partName = "body", targetNamespace = HPDSoapConstants.CHCPI_NAMESPACE) DownloadRequest request) throws MissingArgumentException {
        CPIProvider directory = new CPIProvider(
                Transaction.GetTransactionByKeyword("CIDD"), Actor.findActorWithKeyword("CPI_CONS"), getRemoteHostFromContext());
        if (request.getFromDate() == null) {
            throw new MissingArgumentException("Parameter fromDate is mandatory");
        }
        return directory.handleRequest(request);
    }

    private String getRemoteHostFromContext() {
        HttpServletRequest hRequest = (HttpServletRequest) context.getMessageContext()
                .get(MessageContext.SERVLET_REQUEST);
        return hRequest.getRemoteAddr();
    }


}
