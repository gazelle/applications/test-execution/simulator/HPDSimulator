package net.ihe.gazelle.chcpi.simulator.cpiprovider;

import net.ihe.gazelle.hpd.utils.HPDSoapConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.namespace.QName;
import javax.xml.soap.DetailEntry;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPFault;
import javax.xml.ws.handler.MessageContext;
import javax.xml.ws.handler.soap.SOAPHandler;
import javax.xml.ws.handler.soap.SOAPMessageContext;
import java.util.Iterator;
import java.util.Locale;
import java.util.Set;

public class SoapFaultHandler implements SOAPHandler<SOAPMessageContext> {

    private static Logger log = LoggerFactory.getLogger(SoapFaultHandler.class);

    @Override
    public Set<QName> getHeaders() {
        return null;
    }

    @Override
    public boolean handleMessage(SOAPMessageContext soapMessageContext) {
        return true;
    }

    @Override
    public boolean handleFault(SOAPMessageContext soapMessageContext) {
        processMessage(soapMessageContext);
        return true;
    }

    @Override
    public void close(MessageContext messageContext) {
    }

    public void processMessage(SOAPMessageContext soapMessageContext) {
        Boolean outbound = (Boolean) soapMessageContext.get(MessageContext.MESSAGE_OUTBOUND_PROPERTY);
        if (outbound) {
            try {
                SOAPFault fault = soapMessageContext.getMessage().getSOAPBody().getFault();
                if (isSchemaValidationException(fault)) {
                    Iterator iterator = fault.getDetail().getDetailEntries();
                    while (iterator.hasNext()) {
                        iterator.next();
                        iterator.remove();
                    }
                    String reason = soapMessageContext.getMessage().getSOAPBody().getFault().getFaultReasonText(Locale.ENGLISH);
                    fault.addNamespaceDeclaration("a", HPDSoapConstants.CHCPI_NAMESPACE);
                    QName faultCode = new QName("http://www.w3.org/2003/05/soap-envelope", "Sender");
                    fault.setFaultCode(faultCode);
                    fault.appendFaultSubcode(new QName(HPDSoapConstants.CHCPI_NAMESPACE, "XML_SCHEMA_VIOLATION", "a"));
                    fault.addFaultReasonText(reason, Locale.ENGLISH);
                }

                if("java.lang.IllegalArgumentException: wrong number of arguments".equals(fault.getFaultReasonText(Locale.ENGLISH))){
                    fault.setFaultCode(fault.getFaultCode().replace("Receiver", "Sender"));
                }

                if("Parameter fromDate is mandatory".equals(fault.getFaultReasonText(Locale.ENGLISH))){
                    fault.setFaultCode(fault.getFaultCode().replace("Receiver", "Sender"));
                }
            } catch (SOAPException e) {
                log.error("Cannot modify the SoapFaut : " + e.getMessage());
            }
        }
    }

    private boolean isSchemaValidationException(SOAPFault fault) {
        boolean isSchemaValidationException = false;
        if (fault.getDetail() != null) {
            Iterator iterator = fault.getDetail().getDetailEntries();
            while (iterator.hasNext()) {
                DetailEntry detailEntry = (DetailEntry) iterator.next();
                if (detailEntry.getElementName().getLocalName().equals("SchemaValidationException")) {
                    isSchemaValidationException = true;
                }
            }
        }
        return isSchemaValidationException;
    }
}
