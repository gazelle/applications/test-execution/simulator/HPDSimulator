package net.ihe.gazelle.chcpi.simulator.cpiprovider;

import net.ihe.gazelle.chcpi.action.CIDDMessageDAO;
import net.ihe.gazelle.chcpi.model.CIDDMessage;
import net.ihe.gazelle.chcpi.model.DownloadRequest;
import net.ihe.gazelle.chcpi.model.DownloadResponse;
import net.ihe.gazelle.hpd.BatchRequest;
import net.ihe.gazelle.hpd.BatchResponse;
import net.ihe.gazelle.hpd.simulator.providerinfodirectory.DSMLConnector;
import net.ihe.gazelle.hpd.simulator.providerinfodirectory.ProviderInformationDirectory;
import net.ihe.gazelle.hpd.utils.HPDSoapConstants;
import net.ihe.gazelle.hpd.utils.SAXParserFactoryProvider;
import net.ihe.gazelle.hpd.validator.ValidatorType;
import net.ihe.gazelle.hpdTransformer.HPDTransformer;
import net.ihe.gazelle.simulator.common.tf.model.Actor;
import net.ihe.gazelle.simulator.common.tf.model.Transaction;
import net.ihe.gazelle.simulator.message.model.MessageInstanceMetadata;
import net.ihe.gazelle.validation.DocumentValidXSD;
import net.ihe.gazelle.validation.XSDMessage;
import net.ihe.gazelle.validator.hpd.util.XMLValidation;
import org.jboss.seam.Component;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManager;
import javax.xml.bind.JAXBException;
import javax.xml.namespace.QName;
import javax.xml.soap.SOAPConstants;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPFactory;
import javax.xml.soap.SOAPFault;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.Serializable;
import java.io.StringWriter;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Locale;

/**
 * This class is used for performing the binding between the WS interface and ApacheDS
 *
 * @author aberge
 */
public class CPIProvider extends ProviderInformationDirectory {

    private static final String EPD = "EPD";
    public static final String CIQ_TRANSACTION_KEYWORD = "CIQ";
    private static Logger log = LoggerFactory.getLogger(CPIProvider.class);


    /**
     * Constructor
     *
     * @param inTransaction : IHE transaction performed (depends on soapAction)
     * @param inSutActor    : role played by the SUT (depends on soapAction)
     * @param remoteHost    : the client IP
     */
    public CPIProvider(Transaction inTransaction, Actor inSutActor, String remoteHost) {
        super(inTransaction, inSutActor, remoteHost);
        this.simulatedActor = Actor.findActorWithKeyword("CPI_PROV");
    }

    /**
     * Create a new instance of TransactionInstance, check the content of the incoming message, forward the request to ApacheDS and format the response (to conform to IHE spec) before sending it back
     * to the sender
     *
     * @param request : request received from the SUT
     * @return the batchResponse to be sent back to the SUT
     */
    public BatchResponse handleCPIRequest(BatchRequest request) throws SchemaValidationException {
        // initialize transactionInstance
        EntityManager entityManager = (EntityManager) Component.getInstance("entityManager");
        fillOutRequest(EPD, ValidatorType.CH_CPI_REQUEST, request);
        entityManager.persist(transactionInstance);
        SOAPFault fault = validateAgainstXsd();
        BatchResponse response = null;
        if (fault == null) {
            response = checkRequest(request);
            if (response == null) {
                response = forwardRequestToLDAPServer(request);
            }
            // fill out response
            if (response != null) {
                fillOutResponse(response, HPD_SIMULATOR, "CH - Community Portal Index Response");
            }
            transactionInstance.save(entityManager);
        } else {
            fillOutSoapFault(fault, HPD_SIMULATOR, entityManager);
            MessageInstanceMetadata faultCode = new MessageInstanceMetadata(transactionInstance.getResponse(), "Fault Code", fault.getFaultCode());
            faultCode.save();
            MessageInstanceMetadata faultReason = new MessageInstanceMetadata(transactionInstance.getResponse(), "Fault Reason", fault.getFaultString());
            faultReason.save();
            transactionInstance.save(entityManager);
            try {
                throw new SchemaValidationException(fault.getFaultReasonText(Locale.ENGLISH));
            } catch (SOAPException e) {
                log.error("Cannot get fault reason text");
            }

        }
        return response;
    }

    public DownloadResponse handleRequest(DownloadRequest request) {
        // initialize transactionInstance
        EntityManager entityManager = (EntityManager) Component.getInstance("entityManager");
        fillOutRequest(EPD, ValidatorType.CH_CIDD_REQUEST, request);
        entityManager.persist(transactionInstance);
        DownloadResponse response = new DownloadResponse();
        response.setRequestID(request.getRequestID());
        List<CIDDMessage> ciddMessages = CIDDMessageDAO.queryValidCIDDMessages(request.getFromDate(), request.getToDate());
        for (CIDDMessage ciddMessage : ciddMessages) {
            try {
                InputStream is = new ByteArrayInputStream(ciddMessage.getMessage().getBytes(StandardCharsets.UTF_8));
                BatchRequest batchRequest = HPDTransformer.unmarshallMessage(BatchRequest.class, is);
                response.addBatchRequest(batchRequest);
            } catch (JAXBException e) {
                log.error("Cannot unmarshall CIDD message " + ciddMessage.getName() + " : " + e.getMessage());
            }
        }
        fillOutResponse(response, HPD_SIMULATOR, ValidatorType.CH_CIDD_RESPONSE.getName());
        transactionInstance.save(entityManager);
        return response;
    }

    protected void fillOutRequest(String domainKeyword, ValidatorType validator, DownloadRequest request) {
        try {
            fillOutRequest(domainKeyword, validator);
            transactionInstance.getRequest().setContent(
                    HPDTransformer.getMessageAsByteArray(DownloadRequest.class, request));
        } catch (JAXBException e) {
            log.error("Unable to marshall request: " + e.getMessage());
        }
    }

    /**
     * Not all request types are allowed by IHE (only searchRequest, addRequest, delRequest, modifyRequest and modDNRequest) and in addition, we do not want users to access all the ldap partitions
     * present on the server side
     *
     * @param inRequest
     * @return true if the request can be forwarded to the LDAP server, false otherwise
     */
    @Override
    protected BatchResponse checkRequest(BatchRequest inRequest) {
        boolean ok = true;
        BatchResponse response = new BatchResponse();
        if ((inRequest == null) || inRequest.getBatchRequests().isEmpty()) {
            return response;
        } else {
            for (Serializable requestPart : inRequest.getBatchRequests()) {
                String localName = ((javax.xml.bind.JAXBElement<?>) requestPart).getName().getLocalPart();
                if (transaction.getKeyword().equals(CIQ_TRANSACTION_KEYWORD)) {
                    ok = checkRequestIsSearchRequest(localName, requestPart, response, ok, "CIQ transaction only supports searchRequest");
                }
            }
            if (ok) {
                return null;
            } else {
                return response;
            }
        }
    }

    @Override
    protected BatchResponse forwardRequestToLDAPServer(BatchRequest request, String username, String password) {
        DSMLConnector client = new DSMLConnector(ldapPartition.getServerIP(), ldapPartition.getServerPort(),
                username, password);
        String requestID = request.getRequestID();
        request.setRequestID("123");
        BatchResponse batchResponse = null;
        try {
            batchResponse = client.processRequestManual(request);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        if (batchResponse != null && batchResponse.getRequestID() != null) {
            batchResponse.setRequestID(requestID);
        }
        return batchResponse;
    }

    protected SOAPFault validateAgainstXsd() {
        XMLValidation xmlValidation = new XMLValidation(SAXParserFactoryProvider.getHpdFactory(),
                SAXParserFactoryProvider.getXsd());
        String request = transactionInstance.getRequest().getContentAsString();
        if (request.contains("base64Binary")) {
            request = request.replaceAll("(\\w)*:base64Binary", "xs:base64Binary");
        }
        DocumentValidXSD documentValidXSD = xmlValidation.isXSDValid(request);
        if (!documentValidXSD.getResult().equals("PASSED")) {
            SOAPFault fault = null;
            try {
                StringBuilder stringBuilder = new StringBuilder();
                for (XSDMessage xsdMessage : documentValidXSD.getXSDMessage()) {
                    stringBuilder.append(xsdMessage.getMessage());
                    stringBuilder.append("\n");
                }
                fault = SOAPFactory.newInstance(SOAPConstants.SOAP_1_2_PROTOCOL)
                        .createFault();
                fault.addNamespaceDeclaration("a", HPDSoapConstants.CHCPI_NAMESPACE);
                fault.addFaultReasonText(stringBuilder.toString(), Locale.ENGLISH);
                QName faultCode = new QName("http://www.w3.org/2003/05/soap-envelope", "Sender");
                fault.setFaultCode(faultCode);
                fault.appendFaultSubcode(new QName(HPDSoapConstants.CHCPI_NAMESPACE, "XML_SCHEMA_VIOLATION", "a"));
                return fault;
            } catch (SOAPException e) {
                log.error("Error creating the SOAPFault");
            }
        }
        return null;
    }

    protected DownloadResponse fillOutResponse(DownloadResponse response, String issuer, String responseType) {
        transactionInstance.getResponse().setIssuingActor(simulatedActor);
        transactionInstance.getResponse().setIssuer(issuer);
        transactionInstance.getResponse().setType(responseType);
        try {
            transactionInstance.getResponse().setContent(
                    HPDTransformer.getMessageAsByteArray(DownloadResponse.class, response));
        } catch (JAXBException e) {
            log.error("Unable to marshall response: " + e.getMessage());
        }
        return response;
    }

    protected SOAPFault fillOutSoapFault(SOAPFault fault, String issuer, EntityManager entityManager) {
        transactionInstance.getResponse().setIssuingActor(simulatedActor);
        transactionInstance.getResponse().setIssuer(issuer);
        transactionInstance.getResponse().setType("SOAP Fault");

        StringWriter sw = new StringWriter();
        try {
            TransformerFactory.newInstance().newTransformer().transform(
                    new DOMSource(fault), new StreamResult(sw));
        } catch (TransformerException e) {
            e.printStackTrace();
        }
        String xml = sw.toString();
        transactionInstance.getResponse().setContent(xml);
        return fault;
    }
}
