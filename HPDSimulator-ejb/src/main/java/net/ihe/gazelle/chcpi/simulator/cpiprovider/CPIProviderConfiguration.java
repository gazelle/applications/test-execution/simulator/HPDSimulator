package net.ihe.gazelle.chcpi.simulator.cpiprovider;

import net.ihe.gazelle.hpd.simulator.SimulatorType;
import net.ihe.gazelle.ldap.model.LDAPPartition;
import net.ihe.gazelle.ldap.model.LDAPPartitionQuery;
import net.ihe.gazelle.simulator.common.model.ApplicationConfiguration;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Create;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;

import java.io.Serializable;
import java.util.List;

@Name("cpiProviderConfiguration")
@Scope(ScopeType.PAGE)
public class CPIProviderConfiguration implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 2816725395188368142L;
    private String directoryWsdl;

    @Create
    public void init() {
        this.directoryWsdl = ApplicationConfiguration.getValueOfVariable("cpi_prov_wsdl");
    }

    public String getDirectoryWsdl() {
        return this.directoryWsdl;
    }

    public List<LDAPPartition> getAvailablePartitions() {
        LDAPPartitionQuery query = new LDAPPartitionQuery();
        query.simulator().eq(SimulatorType.CPI_PROV);
        return query.getList();
    }
}
