package net.ihe.gazelle.chcpi.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for DownloadRequest complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="DownloadRequest">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;attribute name="fromDate" use="required" type="{http://www.w3.org/2001/XMLSchema}String" />
 *       &lt;attribute name="toDate" type="{http://www.w3.org/2001/XMLSchema}String" />
 *       &lt;attribute name="requestID" type="{http://www.w3.org/2001/XMLSchema}String" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DownloadRequest", namespace = "urn:ch:admin:bag:epr:2017")
@XmlRootElement(
        namespace = "urn:ch:admin:bag:epr:2017",
        name = "downloadRequest"
)
public class DownloadRequest {

    @XmlAttribute(name = "fromDate", required = true)
    protected String fromDate;
    @XmlAttribute(name = "toDate")
    protected String toDate;
    @XmlAttribute(name = "requestID")
    protected String requestID;

    /**
     * Gets the value of the fromDate property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getFromDate() {
        return fromDate;
    }

    /**
     * Sets the value of the fromDate property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setFromDate(String value) {
        this.fromDate = value;
    }

    /**
     * Gets the value of the toDate property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getToDate() {
        return toDate;
    }

    /**
     * Sets the value of the toDate property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setToDate(String value) {
        this.toDate = value;
    }

    /**
     * Gets the value of the requestID property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getRequestID() {
        return requestID;
    }

    /**
     * Sets the value of the requestID property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setRequestID(String value) {
        this.requestID = value;
    }


}
