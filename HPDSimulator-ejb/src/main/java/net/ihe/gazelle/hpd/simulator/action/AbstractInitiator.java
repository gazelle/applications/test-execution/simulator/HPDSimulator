package net.ihe.gazelle.hpd.simulator.action;

import net.ihe.gazelle.hpd.simulator.model.ProviderInformationDirectorySUTConfiguration;
import net.ihe.gazelle.simulator.common.tf.model.Actor;
import net.ihe.gazelle.simulator.common.tf.model.Transaction;
import net.ihe.gazelle.simulator.message.model.TransactionInstance;

import javax.faces.model.SelectItem;
import java.util.List;

public abstract class AbstractInitiator{

    private ProviderInformationDirectorySUTConfiguration selectedSut;
    private Actor simulatedActor;
    private Transaction selectedTransaction;
    private List<TransactionInstance> messages;

    public abstract void sendMessage();
    public abstract void performAnotherTest();

    public List<TransactionInstance> getMessages() {
        return this.messages;
    }

    public void setMessages(List<TransactionInstance> messages) {
        this.messages = messages;
    }

    public void setSelectedSut(ProviderInformationDirectorySUTConfiguration sut) {
        this.selectedSut = sut;
    }

    public ProviderInformationDirectorySUTConfiguration getSelectedSut() {
        return this.selectedSut;
    }

    public Actor getSimulatedActor() {
        return simulatedActor;
    }

    public void setSimulatedActor(Actor simulatedActor) {
        this.simulatedActor = simulatedActor;
    }

    public Transaction getSelectedTransaction() {
        return selectedTransaction;
    }

    public void setSelectedTransaction(Transaction selectedTransaction) {
        this.selectedTransaction = selectedTransaction;
    }

    public abstract List<ProviderInformationDirectorySUTConfiguration> listAvailableSut();

}
