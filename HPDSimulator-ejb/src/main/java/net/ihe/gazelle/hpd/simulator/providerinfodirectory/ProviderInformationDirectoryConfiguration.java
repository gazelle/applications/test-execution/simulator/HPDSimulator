package net.ihe.gazelle.hpd.simulator.providerinfodirectory;

import java.io.Serializable;
import java.util.List;

import net.ihe.gazelle.hpd.simulator.SimulatorType;
import net.ihe.gazelle.ldap.model.LDAPPartition;
import net.ihe.gazelle.ldap.model.LDAPPartitionQuery;
import net.ihe.gazelle.simulator.common.model.ApplicationConfiguration;

import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Create;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;

@Name("providerInformationDirectoryConfiguration")
@Scope(ScopeType.PAGE)
public class ProviderInformationDirectoryConfiguration implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2816725395188368142L;
	private String directoryWsdl;

	@Create
	public void init() {
		this.directoryWsdl = ApplicationConfiguration.getValueOfVariable("prov_info_dir_wsdl");
	}

	public String getDirectoryWsdl() {
		return this.directoryWsdl;
	}

	public List<LDAPPartition> getAvailablePartitions() {
		LDAPPartitionQuery query = new LDAPPartitionQuery();
		query.simulator().eq(SimulatorType.HPD_PROV_INFO_DIR);
		return query.getList();
	}
}
