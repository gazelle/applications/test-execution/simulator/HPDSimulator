package net.ihe.gazelle.hpd.simulator.admin;

import net.ihe.gazelle.common.filter.list.GazelleListDataModel;
import net.ihe.gazelle.ldap.model.LDAPNode;
import net.ihe.gazelle.ldap.model.LDAPNodeQuery;
import net.ihe.gazelle.ldap.model.ObjectClasses;
import net.ihe.gazelle.ldap.model.ObjectClassesQuery;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Create;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;

import java.io.Serializable;
import java.util.List;

@Name("ldapNodeManager")
@Scope(ScopeType.PAGE)
public class LDAPNodeManager implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -2817449928137397420L;

    private LDAPNode selectedNode;
    private List<LDAPNode> availableNodes;
    private boolean displayList;
    private boolean displayEdit;

    @Create
    public void listAvailableNodes() {
        LDAPNodeQuery query = new LDAPNodeQuery();
        query.name().order(true);
        availableNodes = query.getList();
        this.displayList = true;
        this.displayEdit = false;
        this.selectedNode = null;
    }

    public void editNode(LDAPNode inNode) {
        this.selectedNode = inNode;
        this.displayEdit = true;
        this.displayList = false;
    }

    public void createNewNode() {
        editNode(new LDAPNode());
    }

    public void cancelEdition() {
        this.displayList = true;
        this.displayEdit = false;
        this.selectedNode = null;
    }

    public void saveNode() {
        if (selectedNode != null) {
            // before saving we check that all the superior classes are present
            // if (selectedNode.getObjectClasses() != null && !selectedNode.getObjectClasses().isEmpty()) {
            // Iterator<ObjectClasses> iterator = selectedNode.getObjectClasses().iterator();
            // while (iterator.hasNext()){
            // ObjectClasses testedClass = iterator.next();
            // if (testedClass.getSupobjectclass() != null && !testedClass.getSupobjectclass().isEmpty()){
            // ObjectClassesQuery query = new ObjectClassesQuery();
            // query.name().eq(testedClass.getSupobjectclass());
            // ObjectClasses superiorClass = query.getUniqueResult();
            // if (superiorClass != null && !selectedNode.getObjectClasses().contains(superiorClass)){
            // selectedNode.getObjectClasses().add(superiorClass);
            // }
            // }
            // }
            // }
            selectedNode.save();
            listAvailableNodes();
        }
    }

    public List<ObjectClasses> getAvailableClasses() {
        ObjectClassesQuery query = new ObjectClassesQuery();
        query.name().order(true);
        return query.getList();
    }

    public LDAPNode getSelectedNode() {
        return selectedNode;
    }

    public void setSelectedNode(LDAPNode selectedNode) {
        this.selectedNode = selectedNode;
    }

    public boolean isDisplayList() {
        return displayList;
    }

    public void setDisplayList(boolean displayList) {
        this.displayList = displayList;
    }

    public boolean isDisplayEdit() {
        return displayEdit;
    }

    public void setDisplayEdit(boolean displayEdit) {
        this.displayEdit = displayEdit;
    }

    public List<LDAPNode> getAvailableNodes() {
        return availableNodes;
    }

    public GazelleListDataModel<LDAPNode> getAllAvailableNodes() {
        List<LDAPNode> res = getAvailableNodes();
        GazelleListDataModel<LDAPNode> dm = new GazelleListDataModel<LDAPNode>(res);
        return dm;
    }

}
