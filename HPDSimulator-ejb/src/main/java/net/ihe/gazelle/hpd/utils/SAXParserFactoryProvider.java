package net.ihe.gazelle.hpd.utils;

import javax.ejb.DependsOn;
import javax.ejb.Remove;
import javax.xml.parsers.SAXParserFactory;

import net.ihe.gazelle.common.interfacegenerator.GenerateInterface;
import net.ihe.gazelle.simulator.common.model.ApplicationConfiguration;
import net.ihe.gazelle.validator.hpd.util.XMLValidation;

import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Create;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.Startup;

@Name("saxParserFactoryProvider")
@Scope(ScopeType.APPLICATION)
@Startup(depends={"entityManager"})
@DependsOn({"entityManager"})
@GenerateInterface("SAXParserFactoryProviderLocal")
public class SAXParserFactoryProvider implements SAXParserFactoryProviderLocal {

	private static SAXParserFactory hpdFactory;
	private static String xsd;

	@Override
	@Create
	public void initializeFactories() {
		synchronized (SAXParserFactoryProvider.class) {
			xsd = ApplicationConfiguration.getValueOfVariable("xsd_location");
			hpdFactory = XMLValidation.initializeFactory(xsd);
		}
	}
	
	public static SAXParserFactory getHpdFactory(){
		return hpdFactory;
	}
	
	public static String getXsd(){
		return xsd;
	}
	
	@Override
	@Remove
	public void destroy() {

	}
}
