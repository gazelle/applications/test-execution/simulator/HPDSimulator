package net.ihe.gazelle.hpd.validator;

public enum ProfileType {
    HPD,
    CH_PIDD,
    CH_CIDD;
}
