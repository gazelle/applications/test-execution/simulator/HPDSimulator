package net.ihe.gazelle.hpd.simulator.admin;

import java.io.Serializable;
import java.util.Map;

import javax.faces.context.FacesContext;

import net.ihe.gazelle.common.filter.Filter;
import net.ihe.gazelle.common.filter.FilterDataModel;
import net.ihe.gazelle.hql.HQLQueryBuilder;
import net.ihe.gazelle.hql.criterion.HQLCriterionsForFilter;
import net.ihe.gazelle.hql.criterion.QueryModifier;
import net.ihe.gazelle.hql.restrictions.HQLRestrictions;
import net.ihe.gazelle.ldap.model.AttributeTypes;
import net.ihe.gazelle.ldap.model.ObjectClasses;
import net.ihe.gazelle.ldap.model.ObjectClassesQuery;

import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;

@Name("objectClassesManager")
@Scope(ScopeType.PAGE)
public class ObjectClassesManager implements Serializable, QueryModifier<ObjectClasses> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6578539897810217312L;

	private Filter<ObjectClasses> filter;
	private FilterDataModel<ObjectClasses> data;
	private String attributeName;
	private AttributeTypes selectedAttribute;
	private ObjectClasses selectedObjectClass;

	public Filter<ObjectClasses> getFilter() {
		if (filter == null) {
			filter = new Filter<ObjectClasses>(getHQLCriterions(), FacesContext.getCurrentInstance()
					.getExternalContext().getRequestParameterMap());
		}
		return filter;
	}

	public FilterDataModel<ObjectClasses> getData() {
		if (data == null) {
			data = new FilterDataModel<ObjectClasses>(getFilter()){
                    @Override
        protected Object getId(ObjectClasses t) {
        // TODO Auto-generated method stub
        return t.getId();
        }
        };
		}
		return data;
	}

	private HQLCriterionsForFilter<ObjectClasses> getHQLCriterions() {
		ObjectClassesQuery query = new ObjectClassesQuery();
		HQLCriterionsForFilter<ObjectClasses> criteria = query.getHQLCriterionsForFilter();
		criteria.addPath("name", query.name());
		criteria.addPath("oid", query.oid());
		criteria.addQueryModifier(this);
		return criteria;
	}

	@Override
	public void modifyQuery(HQLQueryBuilder<ObjectClasses> queryBuilder, Map<String, Object> values) {
		if ((attributeName != null) && !attributeName.isEmpty()) {
			queryBuilder.addRestriction(HQLRestrictions.or(HQLRestrictions.like("must", attributeName),
					HQLRestrictions.like("may", attributeName)));
		}
	}

	public String getAttributeName() {
		return attributeName;
	}

	public void setAttributeName(String attributeName) {
		this.attributeName = attributeName;
		filter.modified();
	}

	public void reset() {
		attributeName = null;
		filter.clear();
		data.resetCache();
	}

	public void setSelectedAttributeType(String inName) {
		this.selectedAttribute = AttributeTypes.getAttributeTypeByName(inName);
	}

	public AttributeTypes getSelectedAttribute() {
		return selectedAttribute;
	}

	public ObjectClasses getSelectedObjectClass() {
		return selectedObjectClass;
	}

	public void setSelectedObjectClass(ObjectClasses selectedObjectClass) {
		this.selectedObjectClass = selectedObjectClass;
	}

}
