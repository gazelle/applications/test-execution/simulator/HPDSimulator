package net.ihe.gazelle.hpd.simulator.providerinfoconsumer;

public enum FilterType {
	
	AND ("and", null),
	OR ("or", null),
	NOT ("not", null),
	SUBSTRING ("substrings", null),
	EQUALITY_MATCH ("equalityMatch", "="),
	GREATER_OR_EQUAL ("greaterOrEqual", ">="),
	LESS_OR_EQUAL ("lessOrEqual", "<="),
	PRESENT ("present", null),
	APPROX_MATCH ("approxMatch", "~"),
	EXTENSIBLE_MATCH ("extensibleMatch", null);
	
	String label;

	String assertionTypeSign;
	
	private FilterType(String label, String sign) {
		this.label = label;
		this.assertionTypeSign = sign;
	}
	
	public String getLabel(){
		return this.label;
	}

	public String getAssertionTypeSign() {
		return assertionTypeSign;
	}
}
