package net.ihe.gazelle.hpd.simulator.gui;

import net.ihe.gazelle.hpd.AttributeValueAssertion;
import net.ihe.gazelle.hpd.simulator.providerinfoconsumer.FilterType;

import java.io.Serializable;

/**
 * Created by aberge on 23/06/17.
 */
public class AttributeValueAssertion4GUI extends AbstractFilterItem {

    private AttributeValueAssertion attributeValueAssertion;

    public AttributeValueAssertion4GUI(FilterType inType){
        super();
        this.attributeValueAssertion = new AttributeValueAssertion();
        setFilterType(inType);
    }

    @Override
    public String getIncludeLink() {
        return "/prov_info_cons/valueAssertion.xhtml";
    }

    public AttributeValueAssertion getAttributeValueAssertion() {
        return attributeValueAssertion;
    }

    @Override
    public Serializable getFilterItem() {
        return attributeValueAssertion;
    }
}
