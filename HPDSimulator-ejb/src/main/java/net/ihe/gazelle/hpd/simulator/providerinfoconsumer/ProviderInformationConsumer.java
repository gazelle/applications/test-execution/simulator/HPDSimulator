package net.ihe.gazelle.hpd.simulator.providerinfoconsumer;

import java.io.ByteArrayInputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBException;

import net.ihe.gazelle.hpd.AddRequest;
import net.ihe.gazelle.hpd.AttributeDescription;
import net.ihe.gazelle.hpd.BatchResponse;
import net.ihe.gazelle.hpd.DelRequest;
import net.ihe.gazelle.hpd.DerefAliasesType;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.ModifyRequest;
import net.ihe.gazelle.hpd.OperationType;
import net.ihe.gazelle.hpd.SearchRequest;
import net.ihe.gazelle.hpd.SearchResultEntry;
import net.ihe.gazelle.hpd.simulator.action.AbstractHPDInitiator;
import net.ihe.gazelle.hpd.simulator.action.RequestWrapper;
import net.ihe.gazelle.hpd.simulator.action.RequestType;
import net.ihe.gazelle.hpd.simulator.gui.AbstractFilterItem;
import net.ihe.gazelle.hpd.simulator.gui.AttributeDescription4GUI;
import net.ihe.gazelle.hpd.simulator.gui.AttributeValueAssertion4GUI;
import net.ihe.gazelle.hpd.simulator.gui.FilterSet4GUI;
import net.ihe.gazelle.hpd.simulator.gui.MatchingRuleAssertion4GUI;
import net.ihe.gazelle.hpd.simulator.gui.SubstringFilter4GUI;
import net.ihe.gazelle.hpd.simulator.initiator.ProviderInformationQuerySender;
import net.ihe.gazelle.hpd.simulator.initiator.SoapHPDClient;
import net.ihe.gazelle.hpd.simulator.model.ProviderInformationDirectorySUTConfiguration;
import net.ihe.gazelle.hpd.simulator.model.ProviderInformationDirectorySUTConfigurationQuery;
import net.ihe.gazelle.hpdTransformer.HPDTransformer;
import net.ihe.gazelle.hql.restrictions.HQLRestrictions;
import net.ihe.gazelle.ldap.model.LDAPNode;
import net.ihe.gazelle.simulator.common.tf.model.Actor;
import net.ihe.gazelle.simulator.common.tf.model.Transaction;
import net.ihe.gazelle.simulator.message.model.MessageInstance;
import net.ihe.gazelle.simulator.message.model.TransactionInstance;

import net.ihe.gazelle.simulator.sut.model.SystemConfiguration;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Create;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.contexts.Contexts;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.security.Identity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Name("providerInformationConsumer")
@Scope(ScopeType.PAGE)
public class ProviderInformationConsumer extends AbstractHPDInitiator implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1244574987305123998L;

    private static Logger LOGGER = LoggerFactory.getLogger(ProviderInformationConsumer.class);

    private String attributeName = null;
    private List<RequestWrapper> newRequests = null;


    @Create
    public void init() {
        setSelectedTransaction(Transaction.GetTransactionByKeyword("ITI-58"));
        setSimulatedActor(Actor.findActorWithKeyword("PROV_INFO_CONS"));
        newRequests = null;
    }

    @Override
    public List<ProviderInformationDirectorySUTConfiguration> listAvailableSut() {
        ProviderInformationDirectorySUTConfigurationQuery query = new ProviderInformationDirectorySUTConfigurationQuery();
        if (!Identity.instance().isLoggedIn()) {
            query.isPublic().eq(true);
        } else if (!Identity.instance().hasRole("admin_role")) {
            query.addRestriction(HQLRestrictions.or(HQLRestrictions.eq("isPublic", true),
                    HQLRestrictions.eq("owner", Identity.instance().getCredentials().getUsername())));
        }
        return query.getList();
    }

    @Override
    public void onNodeSelected(RequestWrapper currentRequest) {
        String baseDN = "ou=" + currentRequest.getSelectedNode().getName().concat(",");
        if (getSelectedSut() != null) {
            baseDN = baseDN.concat(getSelectedSut().getBaseDN());
            LOGGER.info("baseDN: " + baseDN);
        }
        ((SearchRequest) currentRequest.getRequest()).setDn(baseDN);
        currentRequest.listAllAvailableAttributes();
        currentRequest.initializeListOfRequestedAttributes();
    }

    public void addAttributeToRequest(RequestWrapper currentRequest) {
        currentRequest.changeAttributeState(attributeName);
        attributeName = null;
    }

    public String getAttributeName(Serializable attribute) {
        return ((AttributeDescription) ((javax.xml.bind.JAXBElement<?>) attribute).getValue()).getName();
    }

    public net.ihe.gazelle.hpd.ScopeType[] getScopeValues() {
        return net.ihe.gazelle.hpd.ScopeType.values();
    }

    public DerefAliasesType[] getDerefAliasesValues() {
        return DerefAliasesType.values();
    }

    public Integer getMaxValue() {
        return Integer.MAX_VALUE;
    }

    public FilterType[] getListOfFilters() {
        return FilterType.values();
    }

    public void appendNewFilter(FilterType filterType, RequestWrapper currentRequest, FilterSet4GUI parent) {
        if (parent == null) {
            currentRequest.setSearchFilter(createFilterForType(filterType));
        } else {
            AbstractFilterItem child = createFilterForType(filterType);
            parent.addItem(child);
        }
    }

    public AbstractFilterItem createFilterForType(FilterType filterType) {
        switch (filterType) {
            case AND:
                return new FilterSet4GUI(filterType);
            case APPROX_MATCH:
                return new AttributeValueAssertion4GUI(filterType);
            case EQUALITY_MATCH:
                return new AttributeValueAssertion4GUI(filterType);
            case EXTENSIBLE_MATCH:
                return new MatchingRuleAssertion4GUI();
            case GREATER_OR_EQUAL:
                return new AttributeValueAssertion4GUI(filterType);
            case LESS_OR_EQUAL:
                return new AttributeValueAssertion4GUI(filterType);
            case OR:
                return new FilterSet4GUI(filterType);
            case PRESENT:
                return new AttributeDescription4GUI();
            case SUBSTRING:
                return new SubstringFilter4GUI();
            default:
                break;
        }
        return null;
    }

    @Override
    protected SoapHPDClient getSoapHPDClient(SystemConfiguration sut) {
        return new ProviderInformationQuerySender(sut);
    }

    @Override
    public void sendMessage() {
        super.sendMessage();
        if (this.getMessages() != null && !this.getMessages().isEmpty()) {
            MessageInstance response = this.getMessages().get(0).getResponse();
        }
    }

    public void deleteValue(String dn, DsmlAttr attribute, String value) {
        if (newRequests == null) {
            newRequests = new ArrayList<RequestWrapper>();
        }
        RequestWrapper request = configureNewRequest(dn, RequestType.MODIFY);
        ModifyRequest modifyRequest = (ModifyRequest) request.getRequest();
        modifyRequest.setDn(dn);
        DsmlModification modif = new DsmlModification();
        modif.setName(attribute.getName());
        modif.setOperation(OperationType.DELETE);
        modif.getValue().add(value);
        modifyRequest.getModification().add(modif);
        newRequests.add(request);
    }

    public void replaceValue(String dn, DsmlAttr attribute) {
        if (newRequests == null) {
            newRequests = new ArrayList<RequestWrapper>();
        }
        RequestWrapper request = configureNewRequest(dn, RequestType.MODIFY);
        ModifyRequest modifyRequest = (ModifyRequest) request.getRequest();
        modifyRequest.setDn(dn);
        DsmlModification modif = new DsmlModification();
        modif.setName(attribute.getName());
        modif.setOperation(OperationType.REPLACE);
        // TODO modif.getValue().add(value);
        modifyRequest.getModification().add(modif);
        newRequests.add(request);
    }

    public void addValue(String dn, DsmlAttr attribute) {
        if (newRequests == null) {
            newRequests = new ArrayList<RequestWrapper>();
        }
        RequestWrapper request = configureNewRequest(dn, RequestType.MODIFY);
        ModifyRequest modifyRequest = (ModifyRequest) request.getRequest();
        modifyRequest.setDn(dn);
        DsmlModification modif = new DsmlModification();
        modif.setName(attribute.getName());
        modif.setOperation(OperationType.ADD);
        // TODO modif.getValue().add(value);
        modifyRequest.getModification().add(modif);
        newRequests.add(request);
    }

    public void deleteEntry(String dn) {
        if (newRequests == null) {
            newRequests = new ArrayList<RequestWrapper>();
        }
        RequestWrapper request = configureNewRequest(dn, RequestType.DEL);
        DelRequest delRequest = (DelRequest) request.getRequest();
        delRequest.setDn(dn);
        newRequests.add(request);
    }

    public void addEntry(SearchResultEntry entry) {
        if (newRequests == null) {
            newRequests = new ArrayList<RequestWrapper>();
        }
        RequestWrapper request = configureNewRequest(entry.getDn(), RequestType.ADD);
        AddRequest addRequest = (AddRequest) request.getRequest();
        addRequest.setDn(entry.getDn());
        addRequest.setAttr(new ArrayList<DsmlAttr>(entry.getAttr()));
        newRequests.add(request);
    }

    private RequestWrapper configureNewRequest(String dn, RequestType requestType) {
        if (newRequests == null) {
            newRequests = new ArrayList<RequestWrapper>();
        }
        RequestWrapper request = new RequestWrapper(index++, requestType);
        request.setDn(dn);
        for (LDAPNode node : getAvailableNodes()) {
            if (dn.contains(node.getName())) {
                request.setSelectedNode(node);
                break;
            }
        }
        return request;
    }

    public void sendNewRequest() {
        List<TransactionInstance> instances = this.sendMessageToSut(getSelectedSut(), newRequests);
        if (instances != null) {
            getMessages().addAll(instances);
        }
        newRequests = null;
    }

    public String modifyNewRequest() {
        Contexts.getSessionContext().set("newBatchRequest", newRequests);
        return "/prov_info_src/simulator.seam";
    }

    @Override
    public void performAnotherTest() {
        super.performAnotherTest();
        setBatchResponse(null);
    }

    /**
     * @param parentFilter
     * @param filterToRemove
     */
    public void removeFilter(FilterSet4GUI parentFilter, AbstractFilterItem filterToRemove) {
        parentFilter.removeItem(filterToRemove);
    }

    public void resetFilter(RequestWrapper request) {
        request.setSearchFilter(null);
    }

    public String getAttributeName() {
        return attributeName;
    }

    public void setAttributeName(String attributeName) {
        this.attributeName = attributeName;
    }

    public List<RequestWrapper> getNewRequests() {
        return newRequests;
    }
}
