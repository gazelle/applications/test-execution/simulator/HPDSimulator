package net.ihe.gazelle.hpd.ws;

import net.ihe.gazelle.hpd.utils.SAXParserFactoryProvider;
import net.ihe.gazelle.hpd.validator.GazelleHPDValidator;
import net.ihe.gazelle.hpd.validator.ProfileType;
import net.ihe.gazelle.hpd.validator.ValidatorType;
import net.ihe.gazelle.metadata.application.MetadataServiceProvider;
import net.ihe.gazelle.simulator.common.model.ApplicationConfiguration;
import net.ihe.gazelle.validation.*;
import net.ihe.gazelle.validation.exception.GazelleValidationException;
import net.ihe.gazelle.validation.model.ValidatorDescription;
import net.ihe.gazelle.validation.ws.AbstractModelBasedValidation;
import net.ihe.gazelle.validator.hpd.util.XMLValidation;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;

import javax.ejb.Stateless;
import javax.jws.WebMethod;
import javax.jws.WebResult;
import javax.jws.WebService;
import java.util.ArrayList;
import java.util.List;

// do not change those annotations, they are useful to keep a consistency between all validation services 
@Stateless
@Name("ModelBasedValidationWS")
@WebService(name = "ModelBasedValidationWS", serviceName = "ModelBasedValidationWSService", portName = "ModelBasedValidationWSPort",
        targetNamespace = "http://ws.mb.validator.gazelle.ihe.net")
public class GazelleHPDModelBasedValidation extends AbstractModelBasedValidation {

    @In(value = "metadataServiceProvider")
    MetadataServiceProvider metadataService;


    @Override
    protected String buildReportOnParsingFailure(GazelleValidationException e, ValidatorDescription selectedValidator) {
        DetailedResult result = new DetailedResult();
        XSDMessage error = new XSDMessage();
        error.setLineNumber(0);
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(e.getMessage());
        if (e.getCause() != null && e.getCause().getMessage() != null) {
            stringBuilder.append(": ");
            stringBuilder.append(e.getCause().getMessage());
        }
        error.setMessage(stringBuilder.toString());
        error.setSeverity("error");
        result.setDocumentValidXSD(new DocumentValidXSD());
        result.getDocumentValidXSD().getXSDMessage().add(error);
        result.getDocumentValidXSD().setResult("FAILED");
        GazelleHPDValidator.addResultHeader(result, (ValidatorType) selectedValidator);
        return GazelleHPDValidator.getDetailedResultAsString(result);
    }

    @Override
    protected String executeValidation(String document, ValidatorDescription validator, boolean extracted) throws GazelleValidationException {
        ValidatorType hpdValidatorType = (ValidatorType) validator;
        DetailedResult result;
        GazelleHPDValidator hpdValidator;
        if (((ValidatorType) validator).getProfileType().equals(ProfileType.CH_CIDD)) {
            String xsd = ApplicationConfiguration.getValueOfVariable("xsd_location_CIDD");
            hpdValidator = new GazelleHPDValidator(XMLValidation.initializeFactory(xsd), xsd);
        } else {
            hpdValidator = new GazelleHPDValidator(SAXParserFactoryProvider.getHpdFactory(),
                    SAXParserFactoryProvider.getXsd());
        }
        result = hpdValidator.validate(document, hpdValidatorType);
        if (result != null) {
            if (extracted) {
                // If the result has been extracted from a document, we need to report it. The line numbers might not match
                String nodeName = hpdValidatorType.getNamespaceURI() + ":" + hpdValidatorType.getRootElement();
                Warning warning = new Warning();
                warning.setTest("root_element_qname");
                warning.setLocation("/");
                warning.setDescription("The validation process has only been applied on the " + nodeName +
                        " element. Line numbers and XPath in this report might differ from the actual number in the submitted file");
                if (result.getMDAValidation() == null) {
                    result.setMDAValidation(new MDAValidation());
                    result.getMDAValidation().setResult("NOT PERFORMED");
                }
                result.getMDAValidation().getWarningOrErrorOrNote().add(0, warning);
            }
            result.getValidationResultsOverview().setValidationServiceName(
                    "Gazelle HPD Validator : " + validator);
            String stringResult = GazelleHPDValidator.getDetailedResultAsString(result);
            addValidatorUsage(hpdValidatorType.getName(), result.getValidationResultsOverview().getValidationTestResult());
            return stringResult;
        } else {
            throw new GazelleValidationException(
                    "The validation has ended in an unexpected way, please contact the tool administrator if the error persists");
        }
    }

    @Override
    protected ValidatorDescription getValidatorByOidOrName(String oidOrName) {
        // FIXME : update the method when OIDs are assigned to the validators
        return ValidatorType.getValidatorTypeByLabel(oidOrName);
    }

    @Override
    protected List<ValidatorDescription> getValidatorsForDescriminator(String descriminator) {
        List<ValidatorDescription> availableValidators = new ArrayList<ValidatorDescription>();
        for (ValidatorType validator : ValidatorType.values()) {
            if (descriminator == null || descriminator.isEmpty() || validator.getDescriminator().equals(descriminator)) {
                availableValidators.add(validator);
            }
        }
        return availableValidators;
    }

    public String getValidatorsAsString() {
        return ValidatorType.values().toString();
    }

    @Override
    @WebMethod
    @WebResult(name = "about")
    public String about() {
        String toolVersion = metadataService.getMetadata().getVersion();
        StringBuilder about = new StringBuilder();
        about.append("Gazelle HPD Simulator");
        if (toolVersion != null) {
            about.append(" (" + toolVersion + ")");
        }
        about.append(" provides a model-based validation service for messages defined in the context of the HPD profile, read more on " +
                "https://gazelle.ihe.net");
        return about.toString();
    }
}
