package net.ihe.gazelle.hpd.simulator.gui;

import net.ihe.gazelle.hpd.AttributeDescription;
import net.ihe.gazelle.hpd.AttributeValueAssertion;
import net.ihe.gazelle.hpd.Filter;
import net.ihe.gazelle.hpd.FilterSet;
import net.ihe.gazelle.hpd.MatchingRuleAssertion;
import net.ihe.gazelle.hpd.SubstringFilter;
import net.ihe.gazelle.hpd.simulator.providerinfoconsumer.FilterType;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by aberge on 23/06/17.
 */
public class FilterSet4GUI extends AbstractFilterItem {

    private List<AbstractFilterItem> filterSetItems;

    public FilterSet4GUI(FilterType inType){
        super();
        setFilterType(inType);
        filterSetItems = new ArrayList<AbstractFilterItem>();
    }

    public void addItem(AbstractFilterItem item){
        filterSetItems.add(item);
    }

    public void removeItem(AbstractFilterItem item){
        filterSetItems.remove(item);
    }

    public List<AbstractFilterItem> getFilterSetItems() {
        return filterSetItems;
    }

    @Override
    public String getIncludeLink() {
        return "/prov_info_cons/filterSet.xhtml";
    }

    @Override
    public Serializable getFilterItem() {
        FilterSet filterSet = new FilterSet();
        for (AbstractFilterItem item : filterSetItems){
            if (item.isIncludeInNot()){
                Filter notFilter = item.createNotFilter();
                filterSet.addNot(notFilter);
            } else {
                Serializable filterItem = item.getFilterItem();
                switch (item.getFilterType()){
                    case AND:
                        filterSet.addAnd((FilterSet) filterItem);
                        break;
                    case OR:
                        filterSet.addOr((FilterSet) filterItem);
                        break;
                    case SUBSTRING:
                        filterSet.addSubstrings((SubstringFilter) filterItem);
                        break;
                    case EQUALITY_MATCH:
                        filterSet.addEqualityMatch((AttributeValueAssertion) filterItem);
                        break;
                    case GREATER_OR_EQUAL:
                        filterSet.addGreaterOrEqual((AttributeValueAssertion) filterItem);
                        break;
                    case LESS_OR_EQUAL:
                        filterSet.addLessOrEqual((AttributeValueAssertion) filterItem);
                        break;
                    case PRESENT:
                        filterSet.addPresent((AttributeDescription) filterItem);
                        break;
                    case APPROX_MATCH:
                        filterSet.addApproxMatch((AttributeValueAssertion) filterItem);
                        break;
                    case EXTENSIBLE_MATCH:
                        filterSet.addExtensibleMatch((MatchingRuleAssertion) filterItem);
                        break;
                }
            }
        }
        return filterSet;
    }
}
