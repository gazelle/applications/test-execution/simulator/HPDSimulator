package net.ihe.gazelle.hpd.simulator.gui;

import net.ihe.gazelle.hpd.MatchingRuleAssertion;
import net.ihe.gazelle.hpd.simulator.providerinfoconsumer.FilterType;

import java.io.Serializable;

/**
 * Created by aberge on 23/06/17.
 */
public class MatchingRuleAssertion4GUI extends AbstractFilterItem {

    private MatchingRuleAssertion matchingRuleAssertion;

    public MatchingRuleAssertion4GUI(){
        super();
        matchingRuleAssertion = new MatchingRuleAssertion();
        setFilterType(FilterType.EXTENSIBLE_MATCH);
    }


    @Override
    public String getIncludeLink() {
        return "/prov_inf_cons/ruleAssertion.xhtml";
    }

    public MatchingRuleAssertion getMatchingRuleAssertion() {
        return matchingRuleAssertion;
    }

    @Override
    public Serializable getFilterItem() {
        return matchingRuleAssertion;
    }
}
