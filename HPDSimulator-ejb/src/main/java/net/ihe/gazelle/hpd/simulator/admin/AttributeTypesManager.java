package net.ihe.gazelle.hpd.simulator.admin;

import java.io.Serializable;

import javax.faces.context.FacesContext;

import net.ihe.gazelle.common.filter.Filter;
import net.ihe.gazelle.common.filter.FilterDataModel;
import net.ihe.gazelle.hql.criterion.HQLCriterionsForFilter;
import net.ihe.gazelle.ldap.model.AttributeTypes;
import net.ihe.gazelle.ldap.model.AttributeTypesQuery;

import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;

@Name("attributeTypesManager")
@Scope(ScopeType.PAGE)
public class AttributeTypesManager implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 6578539897810217312L;

    private Filter<AttributeTypes> filter;
    private FilterDataModel<AttributeTypes> data;
    private AttributeTypes selectedAttribute;

    public Filter<AttributeTypes> getFilter() {
        if (filter == null) {
            filter = new Filter<AttributeTypes>(getHQLCriterions(), FacesContext.getCurrentInstance()
                    .getExternalContext().getRequestParameterMap());
        }
        return filter;
    }

    public FilterDataModel<AttributeTypes> getData() {
        if (data == null) {
            data = new FilterDataModel<AttributeTypes>(getFilter()) {
                @Override
                protected Object getId(AttributeTypes t) {
                    // TODO Auto-generated method stub
                    return t.getId();
                }
            };
        }
        return data;
    }

    private HQLCriterionsForFilter<AttributeTypes> getHQLCriterions() {
        AttributeTypesQuery query = new AttributeTypesQuery();
        HQLCriterionsForFilter<AttributeTypes> criteria = query.getHQLCriterionsForFilter();
        criteria.addPath("name", query.name());
        criteria.addPath("oid", query.oid());
        criteria.addPath("syntax", query.syntax());
        return criteria;
    }

    public void reset() {
        filter.clear();
        data.resetCache();
    }

    public AttributeTypes getSelectedAttribute() {
        return selectedAttribute;
    }

    public void setSelectedAttribute(AttributeTypes selectedAttribute) {
        this.selectedAttribute = selectedAttribute;
    }

}
