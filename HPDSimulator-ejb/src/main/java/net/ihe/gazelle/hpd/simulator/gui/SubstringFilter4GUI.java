package net.ihe.gazelle.hpd.simulator.gui;

import net.ihe.gazelle.hpd.SubstringFilter;
import net.ihe.gazelle.hpd.simulator.providerinfoconsumer.FilterType;

import java.io.Serializable;

/**
 * Created by aberge on 23/06/17.
 */
public class SubstringFilter4GUI extends AbstractFilterItem {

    private SubstringFilter substringFilter;

    public SubstringFilter4GUI(){
        super();
        setFilterType(FilterType.SUBSTRING);
        this.substringFilter = new SubstringFilter();
    }

    public SubstringFilter getSubstringFilter() {
        return substringFilter;
    }

    @Override
    public String getIncludeLink() {
        return "/prov_info_cons/substringFilter.xhtml";
    }

    @Override
    public Serializable getFilterItem() {
        return substringFilter;
    }
}
