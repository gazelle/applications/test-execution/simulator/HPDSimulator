/**
 * 
 */
package net.ihe.gazelle.hpd.simulator.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;

import net.ihe.gazelle.hpd.LDAPResultCode;
import net.ihe.gazelle.hpd.TypeType;

import org.jboss.seam.Component;
import org.jboss.seam.annotations.Name;

/**
 * @author aberge According to IHE, the directory shall not return error messages to the source/consumer actors. As the LDAP directory actually may return errors, we log them in order to help the user
 *         with solving the issues he/she may encounter when testing his/her SUT against the tool
 */

@Entity
@Name("directoryErrorMessage")
@Table(name = "hpd_directory_error_message", schema = "public", uniqueConstraints = @UniqueConstraint(columnNames = "id"))
@SequenceGenerator(name = "hpd_directory_error_message_sequence", sequenceName = "hpd_directory_error_message_id_seq", allocationSize = 1)
public class DirectoryErrorMessage implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2939924471867106348L;

	@Id
	@GeneratedValue(generator = "hpd_directory_error_message_sequence", strategy = GenerationType.SEQUENCE)
	@Column(name = "id", nullable = false, unique = true)
	@NotNull
	private Integer id;

	@Column(name = "code")
	private Integer code;

	@Column(name = "code_meaning")
	private String codeMeaning;

	@Column(name = "message")
	private byte[] message;

	@Column(name = "client")
	private String client;

	@Column(name = "transaction_keyword")
	private String transactionKeyword;

	@Column(name = "messageType")
	private String messageType;

	@Column(name = "transaction_instance_id")
	private Integer transactionInstanceId;

	@Column(name = "request_id")
	private String requestId;

	@Column(name = "timestamp")
	@Temporal(TemporalType.TIMESTAMP)
	private Date timestamp;

	public DirectoryErrorMessage() {
		this.timestamp = new Date();
	}

	public void save() {
		EntityManager entityManager = (EntityManager) Component.getInstance("entityManager");
		entityManager.merge(this);
		entityManager.flush();
	}

	public Integer getCode() {
		return code;
	}

	public void setCode(Integer code) {
		this.code = code;
	}

	public String getMessage() {
		if (message != null) {
			return new String(message);
		} else {
			return null;
		}
	}

	public void setMessage(String message) {
		if (message != null) {
			this.message = message.getBytes();
		} else {
			this.message = null;
		}
	}

	public String getClient() {
		return client;
	}

	public void setClient(String client) {
		this.client = client;
	}

	public Integer getTransactionInstanceId() {
		return transactionInstanceId;
	}

	public void setTransactionInstanceId(Integer transactionInstanceId) {
		this.transactionInstanceId = transactionInstanceId;
	}

	public Date getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(Date timestamp) {
		this.timestamp = timestamp;
	}

	public Integer getId() {
		return id;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = (prime * result) + ((client == null) ? 0 : client.hashCode());
		result = (prime * result) + ((timestamp == null) ? 0 : timestamp.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		DirectoryErrorMessage other = (DirectoryErrorMessage) obj;
		if (client == null) {
			if (other.client != null) {
				return false;
			}
		} else if (!client.equals(other.client)) {
			return false;
		}
		if (timestamp == null) {
			if (other.timestamp != null) {
				return false;
			}
		} else if (!timestamp.equals(other.timestamp)) {
			return false;
		}
		return true;
	}

	public String getRequestId() {
		return requestId;
	}

	public void setRequestId(String requestId) {
		this.requestId = requestId;
	}

	public String getCodeMeaning() {
		return codeMeaning;
	}

	public void setCodeMeaning(LDAPResultCode codeMeaning) {
		if (codeMeaning != null) {
			this.codeMeaning = codeMeaning.value();
		} else {
			this.codeMeaning = null;
		}
	}

	public void setCodeMeaning(TypeType codeMeaning) {
		if (codeMeaning != null) {
			this.codeMeaning = codeMeaning.value();
		} else {
			this.codeMeaning = null;
		}
	}

	public String getTransactionKeyword() {
		return transactionKeyword;
	}

	public void setTransactionKeyword(String transactionKeyword) {
		this.transactionKeyword = transactionKeyword;
	}

	public String getMessageType() {
		return messageType;
	}

	public void setMessageType(String messageType) {
		this.messageType = messageType;
	}

}
