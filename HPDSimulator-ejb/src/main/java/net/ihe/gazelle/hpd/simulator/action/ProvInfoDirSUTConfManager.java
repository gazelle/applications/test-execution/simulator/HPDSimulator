package net.ihe.gazelle.hpd.simulator.action;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;

import net.ihe.gazelle.hpd.simulator.model.ProviderInformationDirectorySUTConfiguration;
import net.ihe.gazelle.hpd.simulator.model.ProviderInformationDirectorySUTConfigurationQuery;
import net.ihe.gazelle.hql.HQLQueryBuilder;
import net.ihe.gazelle.hql.criterion.HQLCriterionsForFilter;
import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.hql.restrictions.HQLRestrictions;
import net.ihe.gazelle.simulator.sut.action.AbstractSystemConfigurationManager;
import net.ihe.gazelle.simulator.sut.model.Usage;
import net.ihe.gazelle.simulator.sut.model.UsageQuery;

import net.ihe.gazelle.ssov7.gum.client.application.UserAttributeCommon;
import net.ihe.gazelle.ssov7.gum.client.application.service.UserService;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage;
import org.jboss.seam.security.Identity;

@Name("provInfoDirSUTConfManager")
@Scope(ScopeType.PAGE)
public class ProvInfoDirSUTConfManager extends
		AbstractSystemConfigurationManager<ProviderInformationDirectorySUTConfiguration> implements Serializable, UserAttributeCommon {

	private static final long serialVersionUID = -8203169928429200398L;

	@In(value="gumUserService")
	private transient UserService userService;

	@Override
	public void deleteSelectedSystemConfiguration() {
		if (selectedSystemConfiguration != null) {
			EntityManager entityManager = EntityManagerService.provideEntityManager();
			ProviderInformationDirectorySUTConfiguration configToRemove = entityManager.find(
					ProviderInformationDirectorySUTConfiguration.class, selectedSystemConfiguration.getId());
			entityManager.remove(configToRemove);
			entityManager.flush();
			selectedSystemConfiguration = null;
			reset();
			FacesMessages.instance().add(StatusMessage.Severity.INFO, "SUT configuration successfully deleted");
		} else {
			FacesMessages.instance().add(StatusMessage.Severity.WARN, "No SUT selected for deletion");
		}
	}

	@Override
	protected HQLCriterionsForFilter<ProviderInformationDirectorySUTConfiguration> getHQLCriterionsForFilter() {
		ProviderInformationDirectorySUTConfigurationQuery query = new ProviderInformationDirectorySUTConfigurationQuery();
		HQLCriterionsForFilter<ProviderInformationDirectorySUTConfiguration> criterionsForFilter = query.getHQLCriterionsForFilter();
		criterionsForFilter.addPath("name", query.name());
		criterionsForFilter.addPath("owner", query.owner());
		criterionsForFilter.addPath("system", query.systemName());
		criterionsForFilter.addQueryModifier(this);
		return criterionsForFilter;
	}

	@Override
	public void copySelectedConfiguration(ProviderInformationDirectorySUTConfiguration inConfiguration) {
		if (inConfiguration != null) {
			selectedSystemConfiguration = new ProviderInformationDirectorySUTConfiguration(inConfiguration);
			editSelectedConfiguration();
		} else {
			FacesMessages.instance().add(StatusMessage.Severity.ERROR, "First select the configuration to copy");
		}
	}

	@Override
	public void addConfiguration() {
		selectedSystemConfiguration = new ProviderInformationDirectorySUTConfiguration();
		displayEditPanel = true;
		displayListPanel = false;
	}

	@Override
	public List<Usage> getPossibleListUsages() {
		UsageQuery query = new UsageQuery();
		return query.getList();
	}

	@Override
	public void modifyQuery(HQLQueryBuilder<ProviderInformationDirectorySUTConfiguration> hqlQueryBuilder, Map<String, Object> map) {
		ProviderInformationDirectorySUTConfigurationQuery query = new ProviderInformationDirectorySUTConfigurationQuery();
		if (!Identity.instance().isLoggedIn()) {
			hqlQueryBuilder.addRestriction(query.isPublic().eqRestriction(true));
		} else if (!Identity.instance().hasRole("admin_role")) {
			hqlQueryBuilder.addRestriction(HQLRestrictions.or(HQLRestrictions.eq("isPublic", true),
					HQLRestrictions.eq("owner", Identity.instance().getCredentials().getUsername())));
		}
		if (filteredUsage != null){
			hqlQueryBuilder.addRestriction(query.listUsages().id().eqRestriction(filteredUsage.getId()));
		}
	}

	@Override
	public String getUserName(String userId) {
		return userService.getUserDisplayNameWithoutException(userId);
	}
}
