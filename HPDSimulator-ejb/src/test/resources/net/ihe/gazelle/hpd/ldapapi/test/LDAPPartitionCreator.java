package net.ihe.gazelle.hpd.ldapapi.test;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

import org.apache.directory.api.ldap.codec.api.LdapApiServiceFactory;
import org.apache.directory.api.ldap.model.entry.DefaultEntry;
import org.apache.directory.api.ldap.model.entry.Entry;
import org.apache.directory.api.ldap.model.exception.LdapException;
import org.apache.directory.api.ldap.model.name.Dn;
import org.apache.directory.api.ldap.model.schema.SchemaManager;
import org.apache.directory.ldap.client.api.LdapConnection;
import org.apache.directory.ldap.client.api.LdapNetworkConnection;
import org.apache.directory.server.core.api.interceptor.context.AddOperationContext;
import org.apache.directory.server.core.partition.impl.btree.jdbm.JdbmIndex;
import org.apache.directory.server.core.partition.impl.btree.jdbm.JdbmPartition;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class LDAPPartitionCreator {

	private static Logger log = LoggerFactory.getLogger(LDAPPartitionCreator.class);

	public static void main(String[] args) throws LdapException, IOException {
		LdapConnection ldapConnection = new LdapNetworkConnection("192.168.20.113", 10389);
		try {
			ldapConnection.connect();
		} catch(Exception e) {
			log.error("Unable to connect to server: " + e.getMessage());
			return;
		}
		SchemaManager schemaManager = ldapConnection.getSchemaManager();
		JdbmPartition jdbmPartition = new JdbmPartition(schemaManager);
		jdbmPartition.setId("test");
		Dn suffixDn = new Dn(schemaManager, "dc=test");
		jdbmPartition.setSuffixDn(suffixDn);
		jdbmPartition.setCacheSize(1000);
		try {
			jdbmPartition.setPartitionPath(new URI("/home/gazelle"));
		} catch (URISyntaxException e) {
			log.error("Error when setting partition path on disk: " + e.getMessage());
		}
		try {
			jdbmPartition.addIndex(new JdbmIndex<String, Entry>("objectClass", false));
			jdbmPartition.addIndex( new JdbmIndex<String, Entry>( "dc", false ) );
			jdbmPartition.initialize();
			Entry contextEntry = new DefaultEntry( schemaManager, "dc=test",
				    "objectClass: top", 
				    "objectClass: organization",
				    "dc: test" );
			jdbmPartition.add(new AddOperationContext(null, contextEntry));
		} catch (Exception e) {
			log.error(e.getMessage());
		}
		
	}
}
