package net.ihe.gazelle.chcpi.model;

import org.junit.Assert;
import org.junit.Test;

import java.text.ParseException;
import java.util.Date;

public class DateAdapterTest {

    private DateAdapter dateAdapter = new DateAdapter();

    @Test
    public void parseYear() throws ParseException {
        String dateToParse = "1997";
        Date date = dateAdapter.unmarshal(dateToParse);

        Assert.assertNotNull(date);
    }

    @Test
    public void parseYearAndMonth() throws ParseException {
        String dateToParse = "1997-07";
        Date date = dateAdapter.unmarshal(dateToParse);

        Assert.assertNotNull(date);
    }

    @Test
    public void CompleteDate() throws ParseException {
        String dateToParse = "1997-07-16";
        Date date = dateAdapter.unmarshal(dateToParse);

        Assert.assertNotNull(date);
    }

    @Test
    public void CompleteDatePlusHoursAndMinutes() throws ParseException {
        String dateToParse = "1997-07-16T19:20+01:00";
        Date date = dateAdapter.unmarshal(dateToParse);

        Assert.assertNotNull(date);
    }

    @Test
    public void CompleteDatePlusHoursAndMinutesAndSeconds() throws ParseException {
        String dateToParse = "1997-07-16T19:20:30+01:00";
        Date date = dateAdapter.unmarshal(dateToParse);

        Assert.assertNotNull(date);
    }

    @Test
    public void CompleteDatePlusHoursAndMinutesAndSecondsAndFraction() throws ParseException {
        String dateToParse = "1997-07-16T19:20:30.45+01:00";
        Date date = dateAdapter.unmarshal(dateToParse);

        Assert.assertNotNull(date);
    }

    @Test
    public void CompleteDatePlusHoursAndMinutesAndSecondsAndFraction2() throws ParseException {
        String dateToParse = "1997-07-16T19:20:30.4+01:00";
        Date date = dateAdapter.unmarshal(dateToParse);

        Assert.assertNotNull(date);
    }

    @Test
    public void CompleteDatePlusHoursAndMinutesAndSecondsAndFraction3() throws ParseException {
        String dateToParse = "1997-07-16T19:20:30.455+01:00";
        Date date = dateAdapter.unmarshal(dateToParse);

        Assert.assertNotNull(date);
    }

    @Test
    public void TimezoneTestZ() throws ParseException {
        String dateToParse = "1997-07-16T19:20:30.455Z";
        Date date = dateAdapter.unmarshal(dateToParse);

        Assert.assertNotNull(date);
    }

    @Test
    public void TimezoneTestMinus() throws ParseException {
        String dateToParse = "1997-07-16T19:20:30.455-01:00";
        Date date = dateAdapter.unmarshal(dateToParse);

        Assert.assertNotNull(date);
    }

    @Test(expected = ParseException.class)
    public void KO() throws ParseException {
        String dateToParse = ":9977-07-16T19:20:30.455+01:00";
        Date date = dateAdapter.unmarshal(dateToParse);

        Assert.assertNotNull(date);
    }
}
