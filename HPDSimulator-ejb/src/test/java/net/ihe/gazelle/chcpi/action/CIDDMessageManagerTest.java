package net.ihe.gazelle.chcpi.action;

import net.ihe.gazelle.chcpi.model.CIDDMessage;
import net.ihe.gazelle.hpd.utils.SAXParserFactoryProvider;
import net.ihe.gazelle.validator.hpd.util.XMLValidation;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

@RunWith(PowerMockRunner.class)
@PrepareForTest({SAXParserFactoryProvider.class})
public class CIDDMessageManagerTest {

    CIDDMessageManager manager = new CIDDMessageManager();

    @Before
    public void setUpMock() {
        PowerMockito.mockStatic(SAXParserFactoryProvider.class);
        String xsdPath = "src/test/resources/xsd/DSMLv2.xsd";
        Mockito.when(SAXParserFactoryProvider.getXsd()).thenReturn(xsdPath);
        Mockito.when(SAXParserFactoryProvider.getHpdFactory()).thenReturn(XMLValidation.initializeFactory(xsdPath));
    }

    @Test
    public void searchRequestNotAllowed() throws IOException {
        CIDDMessage message = new CIDDMessage();
        String requestString = new String(Files.readAllBytes(Paths.get("src/test/resources/ciddMessages/searchRequest.xml")));
        message.setMessage(requestString);
        boolean result = manager.validateWithoutSaving(message);
        Assert.assertFalse(result);
    }

    @Test
    public void requestNotValid() throws IOException {
        CIDDMessage message = new CIDDMessage();
        String requestString = new String(Files.readAllBytes(Paths.get("src/test/resources/ciddMessages/unvalidMessage.xml")));
        message.setMessage(requestString);
        boolean result = manager.validateWithoutSaving(message);
        Assert.assertFalse(result);
    }

    @Test
    @Ignore
    //Issue with marshalling in UT but not in jboss
    public void valid() throws IOException {
        CIDDMessage message = new CIDDMessage();
        String requestString = new String(Files.readAllBytes(Paths.get("src/test/resources/ciddMessages/validCiddMessage.xml")));
        message.setMessage(requestString);
        boolean result = manager.validateWithoutSaving(message);
        Assert.assertTrue(result);
    }

    @Test
    @Ignore
    //Issue with marshalling in UT but not in jboss
    public void prettyPrint() throws IOException {
        CIDDMessage message = new CIDDMessage();
        String requestString = new String(Files.readAllBytes(Paths.get("src/test/resources/ciddMessages/ciddMessageOneLine.xml")));
        String[] lines = requestString.split("\r\n|\r|\n");
        message.setMessage(requestString);
        manager.prettyPrintMessage(message);
        lines = message.getMessage().split("\r\n|\r|\n");
        Assert.assertEquals(15, lines.length);
    }
}
