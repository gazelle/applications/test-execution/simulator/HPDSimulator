package net.ihe.gazelle.hpd.simulator.providerinfodirectory;

import net.ihe.gazelle.hpd.AddRequest;
import net.ihe.gazelle.hpd.AttributeDescription;
import net.ihe.gazelle.hpd.BatchRequest;
import net.ihe.gazelle.hpd.BatchResponse;
import net.ihe.gazelle.hpd.DelRequest;
import net.ihe.gazelle.hpd.DerefAliasesType;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.ErrorResponse;
import net.ihe.gazelle.hpd.Filter;
import net.ihe.gazelle.hpd.LDAPResult;
import net.ihe.gazelle.hpd.ModifyDNRequest;
import net.ihe.gazelle.hpd.ModifyRequest;
import net.ihe.gazelle.hpd.ResultCode;
import net.ihe.gazelle.hpd.ScopeType;
import net.ihe.gazelle.hpd.SearchRequest;
import net.ihe.gazelle.hpd.SearchResponse;
import net.ihe.gazelle.hpd.SearchResultEntry;
import net.ihe.gazelle.hpd.validator.ValidatorType;
import net.ihe.gazelle.ldap.model.LDAPPartition;
import net.ihe.gazelle.preferences.PreferenceService;
import net.ihe.gazelle.simulator.common.tf.model.Actor;
import net.ihe.gazelle.simulator.common.tf.model.Domain;
import net.ihe.gazelle.simulator.common.tf.model.Transaction;
import org.apache.directory.api.ldap.model.message.AddResponse;
import org.apache.directory.api.ldap.model.message.AddResponseImpl;
import org.apache.directory.api.ldap.model.message.LdapResult;
import org.apache.directory.api.ldap.model.message.LdapResultImpl;
import org.apache.directory.api.ldap.model.message.ModifyDnResponse;
import org.apache.directory.api.ldap.model.message.ModifyResponse;
import org.jboss.seam.Component;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import javax.persistence.EntityManager;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

@RunWith(PowerMockRunner.class)
@PrepareForTest({Component.class, Actor.class, PreferenceService.class, Domain.class})
public class ProviderInformationDirectoryTest {

    @Mock
    private EntityManager entityManager;

    ProviderInformationDirectory directory;

    String requestID = "24256";

    @Before
    public void setUpMock() {
        PowerMockito.mockStatic(Component.class);
        PowerMockito.mockStatic(Actor.class);
        PowerMockito.mockStatic(PreferenceService.class);
        PowerMockito.mockStatic(Domain.class);

        Mockito.when(Component.getInstance("entityManager")).thenReturn(entityManager);
        Actor actor = new Actor("PROV_INFO_DIR", "Provider Information Director", "Provider Information Directory");
        Mockito.when(Actor.findActorWithKeyword("PROV_INFO_DIR")).thenReturn(actor);
        Mockito.when(PreferenceService.getBoolean("ch_hpd_enabled")).thenReturn(true);
        Domain domain = new Domain();
        domain.setKeyword("EPD");
        domain.setName("CH EPD");
        domain.setDescription("Elektronische Patient Dossier");
        Mockito.when(Domain.getDomainByKeyword("EPD")).thenReturn(domain);
    }

    @Test
    public void hpdQueryValid() {
        Transaction transaction = new Transaction("ITI-58", "ITI-58", "Provider Information Query");
        Actor actor = new Actor("PROV_INFO_DIR", "Provider Information Director", "Provider Information Directory");
        directory = Mockito.spy(new ProviderInformationDirectory(
                transaction, actor, "::1"));
        BatchRequest request = new BatchRequest();
        SearchRequest searchRequest = new SearchRequest();
        searchRequest.setDn("ou=HCProfessional,dc=HPD,o=BAG,c=CH");
        searchRequest.setRequestID(requestID);
        searchRequest.setScope(ScopeType.WHOLESUBTREE);
        searchRequest.setDerefAliases(DerefAliasesType.NEVERDEREFALIASES);
        AttributeDescription attributeDescription = new AttributeDescription();
        attributeDescription.setName("objectClass");
        Filter filter = new Filter();
        filter.setPresent(attributeDescription);
        searchRequest.setFilter(filter);
        request.addSearchRequest(searchRequest);
        Mockito.when(directory.getLdapPartitionForDn(Mockito.anyString(), Mockito.anyString())).thenReturn(true);


        BatchResponse batchResponseFromLdap = new BatchResponse();
        SearchResponse searchResponse = new SearchResponse();
        SearchResultEntry searchResultEntry = new SearchResultEntry();
        searchResultEntry.setDn("uid=test,ou=HCProfessional,dc=HPD,o=BAG,c=CH");
        DsmlAttr uid = new DsmlAttr("uid", "testuid");
        DsmlAttr status = new DsmlAttr("hpdProviderStatus", "Active");
        DsmlAttr memberOf = new DsmlAttr("memberOf", "cn=gazelle:tumorboard,OU=Relationship,DC=HPD,O=BAG,C=ch");
        DsmlAttr sn = new DsmlAttr("sn", "Test");
        DsmlAttr displayName = new DsmlAttr("displayName", "Test Test");
        DsmlAttr givenName = new DsmlAttr("givenName", "Test");
        searchResultEntry.addAttr(uid);
        searchResultEntry.addAttr(status);
        searchResultEntry.addAttr(memberOf);
        searchResultEntry.addAttr(sn);
        searchResultEntry.addAttr(displayName);
        searchResultEntry.addAttr(givenName);
        searchResponse.addSearchResultEntry(searchResultEntry);
        batchResponseFromLdap.addSearchResponse(searchResponse);
        Mockito.doReturn(batchResponseFromLdap).when(directory).forwardRequestToLDAPServer(request);

        BatchResponse response = directory.handleRequest(request);
        assertNotNull(response);
        assertEquals(batchResponseFromLdap.getSearchResponse(), response.getSearchResponse());
        assertEquals(ValidatorType.CH_QUERY_RESPONSE.getName(), directory.transactionInstance.getResponse().getType());
    }

    @Test
    public void hpdFeedAddValid() {
        Transaction transaction = new Transaction("ITI-59", "ITI-59", "Provider Information Feed");
        Actor actor = new Actor("PROV_INFO_DIR", "Provider Information Director", "Provider Information Directory");
        directory = Mockito.spy(new ProviderInformationDirectory(
                transaction, actor, "::1"));
        BatchRequest request = new BatchRequest();
        AddRequest addRequest = new AddRequest();
        addRequest.setDn("ou=HCProfessional,dc=HPD,o=BAG,c=CH");
        addRequest.setRequestID(requestID);
        request.addAddRequest(addRequest);
        Mockito.when(directory.getLdapPartitionForDn(Mockito.anyString(), Mockito.anyString())).thenReturn(true);


        BatchResponse batchResponseFromLdap = new BatchResponse();
        SearchResponse searchResponse = new SearchResponse();
        LDAPResult ldapResult = new LDAPResult();
        ldapResult.setResultCode(new ResultCode());
        ldapResult.setErrorMessage("errormessage");
        searchResponse.setSearchResultDone(ldapResult);

        batchResponseFromLdap.addSearchResponse(searchResponse);
        Mockito.doReturn(batchResponseFromLdap).when(directory).forwardRequestToLDAPServer(request);

        BatchResponse response = directory.handleRequest(request);
        assertEquals(batchResponseFromLdap.getSearchResponse(), response.getSearchResponse());
        assertEquals(ValidatorType.CH_FEED_RESPONSE.getName(), directory.transactionInstance.getResponse().getType());
    }

    @Test
    public void hpdFeedDelValid() {
        Transaction transaction = new Transaction("ITI-59", "ITI-59", "Provider Information Feed");
        Actor actor = new Actor("PROV_INFO_DIR", "Provider Information Director", "Provider Information Directory");
        directory = Mockito.spy(new ProviderInformationDirectory(
                transaction, actor, "::1"));
        BatchRequest request = new BatchRequest();
        DelRequest delRequest = new DelRequest();
        delRequest.setDn("ou=HCProfessional,dc=HPD,o=BAG,c=CH");
        delRequest.setRequestID(requestID);
        request.addDelRequest(delRequest);
        Mockito.when(directory.getLdapPartitionForDn(Mockito.anyString(), Mockito.anyString())).thenReturn(true);


        BatchResponse batchResponseFromLdap = new BatchResponse();
        SearchResponse searchResponse = new SearchResponse();

        batchResponseFromLdap.addSearchResponse(searchResponse);
        Mockito.doReturn(batchResponseFromLdap).when(directory).forwardRequestToLDAPServer(request);

        BatchResponse response = directory.handleRequest(request);
        assertEquals(batchResponseFromLdap.getSearchResponse(), response.getSearchResponse());
        assertEquals(ValidatorType.CH_FEED_RESPONSE.getName(), directory.transactionInstance.getResponse().getType());
    }

    @Test
    public void hpdFeedModifyValid() {
        Transaction transaction = new Transaction("ITI-59", "ITI-59", "Provider Information Feed");
        Actor actor = new Actor("PROV_INFO_DIR", "Provider Information Director", "Provider Information Directory");
        directory = Mockito.spy(new ProviderInformationDirectory(
                transaction, actor, "::1"));
        BatchRequest request = new BatchRequest();
        ModifyRequest modifyRequest = new ModifyRequest();
        modifyRequest.setDn("ou=HCProfessional,dc=HPD,o=BAG,c=CH");
        modifyRequest.setRequestID(requestID);
        request.addModifyRequest(modifyRequest);
        Mockito.when(directory.getLdapPartitionForDn(Mockito.anyString(), Mockito.anyString())).thenReturn(true);


        BatchResponse batchResponseFromLdap = new BatchResponse();
        LDAPResult ldapResult = new LDAPResult();

        batchResponseFromLdap.addAddResponse(ldapResult);
        Mockito.doReturn(batchResponseFromLdap).when(directory).forwardRequestToLDAPServer(request);

        BatchResponse response = directory.handleRequest(request);
        assertEquals(batchResponseFromLdap.getSearchResponse(), response.getSearchResponse());
        assertEquals(ValidatorType.CH_FEED_RESPONSE.getName(), directory.transactionInstance.getResponse().getType());
    }

    @Test
    public void hpdFeedModDNValid() {
        Transaction transaction = new Transaction("ITI-59", "ITI-59", "Provider Information Feed");
        Actor actor = new Actor("PROV_INFO_DIR", "Provider Information Director", "Provider Information Directory");
        directory = Mockito.spy(new ProviderInformationDirectory(
                transaction, actor, "::1"));
        BatchRequest request = new BatchRequest();
        ModifyDNRequest modifyDNRequest = new ModifyDNRequest();
        modifyDNRequest.setDn("ou=HCProfessional,dc=HPD,o=BAG,c=CH");
        request.addModDNRequest(modifyDNRequest);
        Mockito.when(directory.getLdapPartitionForDn(Mockito.anyString(), Mockito.anyString())).thenReturn(true);


        BatchResponse batchResponseFromLdap = new BatchResponse();
        SearchResponse searchResponse = new SearchResponse();

        batchResponseFromLdap.addSearchResponse(searchResponse);
        Mockito.doReturn(batchResponseFromLdap).when(directory).forwardRequestToLDAPServer(request);

        BatchResponse response = directory.handleRequest(request);
        assertEquals(batchResponseFromLdap.getSearchResponse(), response.getSearchResponse());
        assertEquals(ValidatorType.CH_FEED_RESPONSE.getName(), directory.transactionInstance.getResponse().getType());
    }

    @Test
    public void hpdErrorResponse() {
        Transaction transaction = new Transaction("ITI-59", "ITI-59", "Provider Information Feed");
        Actor actor = new Actor("PROV_INFO_DIR", "Provider Information Director", "Provider Information Directory");
        directory = Mockito.spy(new ProviderInformationDirectory(
                transaction, actor, "::1"));
        BatchRequest request = new BatchRequest();
        ModifyDNRequest modifyDNRequest = new ModifyDNRequest();
        modifyDNRequest.setDn("ou=HCProfessional,dc=HPD,o=BAG,c=CH");
        request.addModDNRequest(modifyDNRequest);
        Mockito.when(directory.getLdapPartitionForDn(Mockito.anyString(), Mockito.anyString())).thenReturn(true);


        BatchResponse batchResponseFromLdap = new BatchResponse();
        ErrorResponse errorResponse = new ErrorResponse();

        batchResponseFromLdap.addErrorResponse(errorResponse);
        Mockito.doReturn(batchResponseFromLdap).when(directory).forwardRequestToLDAPServer(request);

        BatchResponse response = directory.handleRequest(request);
        assertEquals(batchResponseFromLdap.getErrorResponse(), response.getErrorResponse());
        assertEquals(ValidatorType.CH_FEED_RESPONSE.getName(), directory.transactionInstance.getResponse().getType());
    }

    @Test
    public void getLdapPartitionForDnQueryTest(){
        Transaction transaction = new Transaction("ITI-58", "ITI-58", "Provider Information Query");
        Actor actor = new Actor("PROV_INFO_DIR", "Provider Information Director", "Provider Information Directory");
        directory = Mockito.spy(new ProviderInformationDirectory(
                transaction, actor, "::1"));
        Mockito.doNothing().when(directory).setPartitionForDn(Mockito.anyString());
        LDAPPartition ldapPartition = new LDAPPartition();
        ldapPartition.setName("Gazelle HPD Directory");
        ldapPartition.setBaseDN("dc=HPD,o=IHE-Europe,c=FRA");
        ldapPartition.setDnSearchRegex("ou=(HCRegulatedOrganization|HCProfessional|HCRelationship),dc=HPD,o=IHE-Europe,c=FRA");
        ldapPartition.setDnModifyRegex("uid=.*,ou=(HCRegulatedOrganization|HCProfessional|HCRelationship),dc=HPD,o=IHE-Europe,c=FRA");
        directory.ldapPartition = ldapPartition;
        assertEquals(true, directory.getLdapPartitionForDn("ou=HCProfessional,dc=HPD,o=IHE-Europe,c=FRA", "searchRequest"));
    }

    @Test
    public void getLdapPartitionForDnFeedTest(){
        Transaction transaction = new Transaction("ITI-59", "ITI-59", "Provider Information Feed");
        Actor actor = new Actor("PROV_INFO_DIR", "Provider Information Director", "Provider Information Directory");
        directory = Mockito.spy(new ProviderInformationDirectory(
                transaction, actor, "::1"));
        Mockito.doNothing().when(directory).setPartitionForDn(Mockito.anyString());
        LDAPPartition ldapPartition = new LDAPPartition();
        ldapPartition.setName("Gazelle HPD Directory");
        ldapPartition.setBaseDN("dc=HPD,o=IHE-Europe,c=FRA");
        ldapPartition.setDnSearchRegex("ou=(HCRegulatedOrganization|HCProfessional|HCRelationship),dc=HPD,o=IHE-Europe,c=FRA");
        ldapPartition.setDnModifyRegex("uid=.*,ou=(HCRegulatedOrganization|HCProfessional|HCRelationship),dc=HPD,o=IHE-Europe,c=FRA");
        directory.ldapPartition = ldapPartition;
        assertEquals(true, directory.getLdapPartitionForDn("uid=test,ou=HCProfessional,dc=HPD,o=IHE-Europe,c=FRA", "addRequest"));
    }

    @Test
    public void getLdapPartitionForDnNullPartitionTest(){
        Transaction transaction = new Transaction("ITI-59", "ITI-59", "Provider Information Feed");
        Actor actor = new Actor("PROV_INFO_DIR", "Provider Information Director", "Provider Information Directory");
        directory = Mockito.spy(new ProviderInformationDirectory(
                transaction, actor, "::1"));
        Mockito.doNothing().when(directory).setPartitionForDn(Mockito.anyString());
        directory.ldapPartition = null;
        assertEquals(false, directory.getLdapPartitionForDn("uid=test,ou=HCProfessional,dc=HPD,o=IHE-Europe,c=FRA", "addRequest"));
    }

    @Test
    public void getLdapPartitionForDnWithoutDn(){
        Transaction transaction = new Transaction("ITI-59", "ITI-59", "Provider Information Feed");
        Actor actor = new Actor("PROV_INFO_DIR", "Provider Information Director", "Provider Information Directory");
        directory = Mockito.spy(new ProviderInformationDirectory(
                transaction, actor, "::1"));
        assertEquals(false, directory.getLdapPartitionForDn(null, "addRequest"));
        assertEquals(null, directory.ldapPartition);
    }

    @Test
    public void getLdapPartitionForDnWrongDn(){
        Transaction transaction = new Transaction("ITI-59", "ITI-59", "Provider Information Feed");
        Actor actor = new Actor("PROV_INFO_DIR", "Provider Information Director", "Provider Information Directory");
        directory = Mockito.spy(new ProviderInformationDirectory(
                transaction, actor, "::1"));
        Mockito.doNothing().when(directory).setPartitionForDn(Mockito.anyString());
        directory.ldapPartition = null;
        assertEquals(false, directory.getLdapPartitionForDn("o=IHE-Europe,c=FRA", "addRequest"));
    }
}
